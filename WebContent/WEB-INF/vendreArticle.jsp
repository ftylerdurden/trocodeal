<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="/WEB-INF/includes/navbar.jsp"></jsp:include>



<div class="container">
	<!-- Jumbotron Header-->
	<section class="jumbotron my-4">
		<h1 class="row justify-content-center">Ajouter un article à
			vendre</h1>
		<br> <br>

		<div class="row justify-content-center">
			<div class="row">
				<c:if test="${!empty StringListeCodesErreurs}">

					<div class="col-12 row justify-content-center">
						<c:forEach var="l" items="${StringListeCodesErreurs}">
							<div style="color: red; font-size: 1.5em;"
								class="col-12 text-center">
								<p>${l}</p>
							</div>
						</c:forEach>
					</div>
				</c:if>
			</div>
		</div>
		<br> <br>
		<c:choose>
         <c:when test = "${article == null}">
			<form action="${pageContext.servletContext.contextPath}/vendreArticle"
				method="post">
	
				<c:if test="${!dateDebut }">
					<div class="row">
						<div class="col">
							<div class="row justify-content-start">
								<div class="col-lg-12 col-md-12 mb-12">
									<label>Nom de l'article : </label> <input class="form-control"
										type="text" name="nom" value="${article.nom}"
										required="required" placeholder="nom de l'article"><br>
								</div>
							</div>
						</div>
					</div>
				</c:if>
				<div class="row">
					<div class="col">
						<div class="row justify-content-start">
							<div class="col-lg-12 col-md-12 mb-12">
								<label>Déscription : </label>
								<textarea class="form-control" name="description"
									<c:out value="${article.getDescription()}" /> rows="5" cols="33"></textarea>
								<br>
							</div>
	
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col">
						<div class="row justify-content-start">
							<div class="col-lg-6 col-md-6 mb-12">
								<label>date de debut de l'encheres : </label> <input
									class="form-control" type="date" name="dateDebutEncheres"
									value="${article.dateDebutEncheres}" required="required"><br>
							</div>
							<div class="col-lg-6 col-md-6 mb-12">
								<label>Heure : </label> <input class="form-control" type="time"
									name="dateDebutEncheresTime"
									value="${article.dateDebutEncheres}" required="required"><br>
							</div>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col">
						<div class="row justify-content-start">
							<div class="col-lg-6 col-md-6 mb-12">
								<label>date de fin de l'encheres : </label> <input
									class="form-control" type="date" name="dateFinEncheres"
									value="${article.dateFinEncheres}" required="required"><br>
							</div>
							<div class="col-lg-6 col-md-6 mb-12">
								<label>Heure : </label> <input class="form-control" type="time"
									name="dateFinEncheresTime" value="${article.dateFinEncheres}"
									required="required"><br>
							</div>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col">
						<div class="row justify-content-start">
							<div class="col-lg-6 col-md-6 mb-12">
								<label for="categorie">Categorie</label> <select name="categorie"
									class="custom-select" required>
									<option value="">-- CHOIX CATEGORIE --</option>
									<c:forEach var="categories" items="${categories}">
										<option value="${categories.getId() }">${categories.getLibelle()}</option>
									</c:forEach>
								</select>
								<div class="invalid-feedback">Selection obligatoire</div>
							</div>
							<div class="col-lg-6 col-md-6 mb-12">
								<label for="miseAPrix">Mise à prix</label> <input
									class="form-control" type="number" id="miseAPrix"
									name="miseAPrix" placeholder="Prix proposé"
									value="<c:out value="${article.miseAPrix }"/>" />
							</div>
						</div>
					</div>
				</div>
	
	
				<div class="row">
					<div class="col">
						<div class="row justify-content-start"></div>
					</div>
				</div>
				<br>
				<hr>

         </c:when>
         <c:otherwise>
			<form action="${pageContext.servletContext.contextPath}/vendreArticle"
				method="post">
	
				<c:if test="${!dateDebut }">
					<div class="row">
						<div class="col">
							<div class="row justify-content-start">
								<div class="col-lg-12 col-md-12 mb-12">
									<label>Nom de l'article : </label> <input class="form-control"
										type="text" name="nom" value="${article.nom}"
										required="required" placeholder="${article.nom}"><br>
								</div>
							</div>
						</div>
					</div>
				</c:if>
				<div class="row">
					<div class="col">
						<div class="row justify-content-start">
							<div class="col-lg-12 col-md-12 mb-12">
								<label>Déscription : </label>
								<textarea class="form-control"  placeholder="${article.getDescription()}" name="description"  
									<c:out value="${article.getDescription()}" /> rows="5" cols="33"></textarea>
								<br>
							</div>
	
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col">
						<div class="row justify-content-start">
							<div class="col-lg-6 col-md-6 mb-12">
								<label>date de debut de l'encheres : </label> <input
									class="form-control" type="date" name="dateDebutEncheres"
									value="${article.dateDebutEncheres}" placeholder="${article.getDescription()}" required="required"><br>
							</div>
							<div class="col-lg-6 col-md-6 mb-12">
								<label>Heure : </label> <input class="form-control" type="time"
									name="dateDebutEncheresTime"
									value="${article.dateDebutEncheres}" placeHolder="${article.dateDebutEncheres}" required="required"><br>
							</div>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col">
						<div class="row justify-content-start">
							<div class="col-lg-6 col-md-6 mb-12">
								<label>date de fin de l'encheres : </label> <input
									class="form-control" type="date" name="dateFinEncheres"
									value="${article.dateFinEncheres}" placeHolder="${article.dateFinEncheres}" required="required"><br>
							</div>
							<div class="col-lg-6 col-md-6 mb-12">
								<label>Heure : </label> <input class="form-control" type="time"
									name="dateFinEncheresTime" value="${article.dateFinEncheres}" placeHolder="${article.dateFinEncheres}"
									required="required"><br>
							</div>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col">
						<div class="row justify-content-start">
							<div class="col-lg-6 col-md-6 mb-12">
								<label for="categorie">Categorie</label> <select name="categorie"
									class="custom-select" required>
									<option value="">-- CHOIX CATEGORIE --</option>
									<c:forEach var="categories" items="${categories}">
										<option value="${categories.getId() }">${categories.getLibelle()}</option>
									</c:forEach>
								</select>
								<div class="invalid-feedback">Selection obligatoire</div>
							</div>
							<div class="col-lg-6 col-md-6 mb-12">
								<label for="miseAPrix">Mise à prix</label> <input
									class="form-control" type="number" id="miseAPrix"
									name="miseAPrix" placeholder="${article.miseAPrix }"
									value="<c:out value="${article.miseAPrix }"/>" />
							</div>
						</div>
					</div>
				</div>
	
	
				<div class="row">
					<div class="col">
						<div class="row justify-content-start"></div>
					</div>
				</div>
				<br>
				<hr>
         </c:otherwise>
      </c:choose>
			<h3>Retrait de l'article</h3>
			<div class="row">
				<div class="col">
					<div class="row justify-content-start">
						<div class="col-lg-6 col-md-12 mb-12">
							<label for="rue">Rue</label> <input class="form-control"
								type="text" id="rue" name="rue" placeholder="Rue"
								value="${utilisateur.rue}" /><br>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="row justify-content-start">
						<div class="col-lg-6 col-md-12 mb-12">
							<label for="codePostal">Code postal</label> <input
								class="form-control" type="text" id="codePostal"
								name="codePostal" placeholder="Code postal"
								value="${utilisateur.codePostal}" /><br>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="row justify-content-start">
						<div class="col-lg-6 col-md-12 mb-12">
							<label for="ville">Ville</label> <input class="form-control"
								type="text" id="ville" name="ville" placeholder="Ville"
								value="${utilisateur.ville }" /><br>
						</div>
					</div>
				</div>
			</div>

			<br>
			<hr>

			<div class="row d-flex justify-content-center">
				<div style="margin: 15px">
					<input class="btn btn-primary btn-lg btn-block" type="submit"
						value="Valider">
				</div>
				<c:if test="${displayAnulerVente ==1 }">
					<div style="margin: 15px">
						<a href="${pageContext.servletContext.contextPath}/accueilEncheres">
							<button class="btn btn-secondary btn-lg btn-block" type="button">Annuler</button>
						</a>
					</div>
				</c:if>
			</div>
		</form>
	</section>
</div>
<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
</body>
</html>