<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="/WEB-INF/includes/navbar.jsp"></jsp:include>
<div class="container">
 <section class="jumbotron my-4">
    <h1>Liste des enchères</h1>
    <div id="recherche-div">
        <h2>Filtres</h2>
        <form action="" method="post" class="rechercheForm">
            <!-- Filtres-->
            <div class="row">
                <div class="col-6">
                    <!-- Recherche-->
                    <div class="row">
                        <div class="col">
                            <input type="text" name="recherche" id="recherche"
                                placeholder="Le nom de l'article contient">
                        </div>
                    </div>
                    <!-- Categories -->
                    <div class="row">
                        <div class="col">
                            <select name="categorie" id="cat-select">
                                <option value="">Toutes</option>
                                <c:forEach var="categorie" items="${categories}">
                                    <option value="${categorie.getId()}">${categorie.getLibelle()}
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <!-- Filtre achat / ventes -->
                    <div class="row">
                        <form>
                            <!-- Achats -->
										<div class="col-6">
											<input type="radio" id="achats" name="achatVente" 
											onclick="location.href = '${pageContext.request.contextPath}/accueilEncheres';">
    										<label for="achats">Achats</label>
    										
    										<div class="achats">
    											<div>
	    											<input type="checkbox" id="encheresOuvertes" name="encheresOuvertes" disabled>
	  												<label for="encheresOuvertes">enchères ouvertes</label>
  												</div>
  												<div>
	  												<input type="checkbox" id="mesEncheres" name="mesEncheres" disabled>
	  												<label for="mesEncheres">mes enchères</label>
  												</div>
  												<div>
	  												<input type="checkbox" id="encheresRemportees" name="encheresRemportees" disabled>
	  												<label for="encheresRemportees">mes enchères remportées</label>
  												</div>
    										</div>
										</div>
										<!-- Ventes -->
										<div class="col-6">
											<input type="radio" id="mesVentes" name="achatVente"  checked="checked">
    										<label for="mesVentes">Mes ventes</label>
    										
    										 <div class="mes-ventes">
    										 	<div>
	    											<input type="radio" id="encheresOuvertes" name="mesVentesFiltre"
	    											onclick="location.href = '${pageContext.request.contextPath}/listeEncheresMesVentes?mesVentesFiltre=1';">
	  												<label for="encheresOuvertes">mes ventes en cours</label>
  												</div>
  												<div>
	  												<input type="radio" id="mesEncheres" name="mesVentesFiltre" 
	  												onclick="location.href = '${pageContext.request.contextPath}/listeEncheresMesVentes?mesVentesFiltre=2';">
	  												<label for="mesEncheres">ventes non débutées</label>
  												</div>
  												<div>
	  												<input type="radio" id="encheresRemportees" name="mesVentesFiltre" 
													onclick="location.href = '${pageContext.request.contextPath}/listeEncheresMesVentes?mesVentesFiltre=3';">
	  												<label for="encheresRemportees">ventes terminées</label>
  												</div>
    										</div>
										</div>
									</form>
								</div>
							</div>
							
						   <!-- Bouton rechercher-->
                <div class="col-6">
                    <input type="submit" value="Rechercher">
                </div>
            </div>
        </form>
    </div>

    <!--Affichage des articles-->
    <div class="row">
        <div class="col-md-12">
            <c:forEach var="articles" items="${ventesEncours}">
                <div class="row">
                    <div class="col-md-6">
                        <img src="">
                    </div>
                    <div class="col-md-6">
                        <h2>
                            <a
                                href="${pageContext.request.contextPath}/encherir?idArticle=${articles.getId()}">${articles.getNom()}</a>
                        </h2>
                        <p>
                            Prix : ${articles.getMiseAPrix()}
                        </p>
                        <p>
                            Fin de l'enchère: ${articles.getDateFinEncheres()}
                        </p>
                        <p>
                            Vendeur: ${articles.getUtilisateur().getNom()}
                        </p>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</section>
</div>
			
		<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
		</body>

		</html>