<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ page session="true"%>

<jsp:include page="/WEB-INF/includes/navbar.jsp"></jsp:include>

<div class="container">
	<!-- Jumbotron Header-->
	<section class="jumbotron my-4">
		<h1 class="row justify-content-center" style="text-align: center;">Modifier mon mot de passe</h1>
		<br> <br>

		<div class="row justify-content-center">
			<div class="row">
				<c:if test="${!empty StringListeCodesErreurs}">

					<div class="col-12 row justify-content-center">
						<c:forEach var="l" items="${StringListeCodesErreurs}">
							<div style="color: red; font-size: 1.5em;"
								class="col-12 text-center">
								<p>${l}</p>
							</div>
						</c:forEach>
					</div>
				</c:if>
			</div>
		</div>
		<br> <br>

		<form
			action="${pageContext.servletContext.contextPath}/modifierMotDePasse"
			method="post">

			<div class="row">
				<div class="col">
					<div class="row justify-content-start">
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Mot de passe actuel : </label> <input class="form-control"
								type="password" name="motDePasse"
								value="${utilisateur.motDePasse}"
								placeholder="Veuillez entrer votre mot de passe actuel"><br>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col">
					<div class="row justify-content-center">
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Nouveau mot de passe : </label> <input
								class="form-control" type="password" name="nouveauMotDePasse"
								value="${utilisateur.motDePasse}"
								placeholder="Veuillez entrer votre nouveau mot de passe"><br>
						</div>
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Confirmation :</label> <input class="form-control"
								type="password" name="confirmation"
								placeholder="Veuillez confirmer votre mot de passe"><br>
						</div>
					</div>
				</div>
			</div>


			<br> <br>


			<div class="row">
				<div class="col-lg-4 col-md-10 mx-auto">
					<input class="btn btn-primary btn-lg btn-block" type="submit"
						value="Enregistrer"><br>
				</div>
				<div class="col-lg-4 col-md-10 mx-auto">
					<a class="btn btn-secondary btn-lg btn-block"
						data-cke-saved-href="annuler" role="button"
						href="${pageContext.servletContext.contextPath}/profil?id=${utilisateur.id}">Annuler</a>
				</div>
			</div>

		</form>
	</section>
</div>
<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
</body>
</html>