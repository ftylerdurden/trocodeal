<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page session="true"%>


<jsp:include page="/WEB-INF/includes/navbar.jsp"></jsp:include>

<div class="container">
	<!-- Jumbotron Header-->
	<section class="jumbotron my-4">
		<h1 class="row justify-content-center">Modifier mon profil</h1>
		<br> <br>

		<div class="row justify-content-center">
			<div class="row">
				<c:if test="${!empty StringListeCodesErreurs}">

					<div class="col-12 row justify-content-center">
						<c:forEach var="l" items="${StringListeCodesErreurs}">
							<div style="color: red; font-size: 1.5em;"
								class="col-12 text-center">
								<p>${l}</p>
							</div>
						</c:forEach>
					</div>
				</c:if>
			</div>
		</div>
		<br> <br>

		<form
			action="${pageContext.servletContext.contextPath}/modifierProfil"
			method="post">
			<div class="row">
				<div class="col">
					<div class="row justify-content-start">
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Pseudo : </label> <input class="form-control" type="text"
								name="pseudo" value="${utilisateur.pseudo}" required="required"
								placeholder="ex. JaneDoe23"><br>
						</div>
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Nom : </label> <input class="form-control" type="text"
								name="nom" value="${utilisateur.nom}" required="required"
								placeholder="ex. Doe"><br>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col">
					<div class="row justify-content-start">
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Prénom : </label> <input class="form-control" type="text"
								name="prenom" value="${utilisateur.prenom}" required="required"
								placeholder="ex. Jane"><br>
						</div>
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Email : </label> <input class="form-control" type="email"
								name="email" value="${utilisateur.email}" required="required"
								placeholder="ex. jane.doe@lost.gone "><br>

						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col">
					<div class="row justify-content-start">
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Téléphone : </label> <input class="form-control"
								type="text" name="telephone" value="${utilisateur.telephone}"
								required="required" placeholder="ex. 06.69.33.69.33"><br>
						</div>
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Rue : </label> <input class="form-control" type="text"
								name="rue" value="${utilisateur.rue}" required="required"
								placeholder="ex. 4 rue des crocodiles"><br>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col">
					<div class="row justify-content-start">
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Code postal : </label> <input class="form-control"
								type="text" name="codePostal" value="${utilisateur.codePostal}"
								required="required" placeholder="ex. 35000"><br>
						</div>
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Ville : </label> <input class="form-control" type="text"
								name="ville" value="${utilisateur.ville}" required="required"
								placeholder="ex. Potts County"><br>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col">
					<div class="row justify-content-start">
						<div class="col-lg-6 col-md-6 mb-12">
							<h5>Crédit : ${utilisateur.credit}</h5>
						</div>
					</div>
				</div>
			</div>
			<br> <br>

			<div class="row">
				<div class="col-lg-4 col-md-10 mx-auto">
					<input class="btn btn-primary btn-lg btn-block" type="submit"
						value="Enregistrer"><br>
				</div>
				<div class="col-lg-4 col-md-10 mx-auto">
					<a class="btn btn-secondary btn-lg btn-block"
						data-cke-saved-href="annuler" role="button"
						href="${pageContext.servletContext.contextPath}/profil?id=${utilisateur.id}">Annuler</a>
				</div>
			</div>

			<br>
			<h3 style="text-align: center">Ou</h3>
			<br>
			<!-- 			<div class="row"> -->
			<!-- 				<div class="col-lg-6 col-md-10 mx-auto"> -->
			<!-- 					<a class="btn btn-danger btn-lg btn-block" -->
			<!-- 						data-cke-saved-href="supprimerCompte" role="button" -->
			<%-- 						href="${pageContext.servletContext.contextPath}/supprimerCompte" --%>
			<!-- 						role="button">Supprimer mon compte</a> -->
			<!-- 				</div> -->
			<!-- 			</div> -->

		</form>
		<div class="row">
				<div class="col-lg-6 col-md-10 mx-auto">
					<button type="button" class="btn btn-danger btn-lg btn-block"
						data-toggle="modal" data-target="#supprimerCompte">Supprimer
						mon compte</button>

					<!-- Modal -->
					<div class="modal fade" id="supprimerCompte" tabindex="-1"
						role="dialog" aria-labelledby="exampleModalLabel"
						aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">${utilisateur.prenom}, êtes-vous
										certain de vouloir supprimer votre compte Trocodeal ?</h5>
									<button type="button" class="close" data-dismiss="modal"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									Cette opération est irréversible.<br> Nous espérons
									cependant vous revoir bientôt ! :)
								</div>
								<div class="modal-footer">
									<form method="get"
										action="${pageContext.request.contextPath}/supprimerCompte">
										<button type="submit" class="btn btn-danger">Supprimer
											définitivement</button>
									</form>
									<button type="button" class="btn btn-primary"
										data-dismiss="modal">Annuler</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</section>
</div>
<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
</body>
</html>