<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="/WEB-INF/includes/navbar.jsp"></jsp:include>
<div class="container">
    <!-- Jumbotron Header-->
    <section class="jumbotron my-4">

        <div class="row justify-content-center">
            <div class="row">
                <c:if test="${!empty stringListeCodesErreurs}">

                    <div class="col-12 row justify-content-center">
                        <c:forEach var="l" items="${stringListeCodesErreurs}">
                            <div style="color: red; font-size: 1.5em;" class="col-12 text-center">
                                <p>${l}</p>
                            </div>
                        </c:forEach>
                    </div>
                </c:if>
            </div>
        </div>
        <br> <br>

        <form action="${pageContext.servletContext.contextPath}/encherir" method="post">
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div  class="col-lg-4 col-md- 2 mb-12" >
                            <img width="300px"  src="images/LogoTrocoDeal2.png" style="border:solid 1px #000000">
                        </div>
                        <div class="col-lg-5 col-md-5 mb-12">
                            <h2>${enchere.getArticle().getNom()}</h2>
                            <p>Description : ${enchere.getArticle().getDescription()}</p>
                            <p>Catégorie : ${enchere.getArticle().getCategorie().getLibelle()}</p>
                            <c:if test="${enchere.getUtilisateur()!=null}">
	                            <p>Meilleure offre : ${enchere.getMontant()} pts par
	                                ${enchere.getUtilisateur().getPseudo()}</p>
                            </c:if>
                            <p>Mise à prix : ${enchere.getArticle().getMiseAPrix()} points</p>
                            <p>Début de l'enchère: ${enchere.getArticle().getDateDebutEncheres()}</p>
                            <p>Fin de l'enchère: ${enchere.getArticle().getDateFinEncheres()}</p>
                            <p>Retrait : ${enchere.getArticle().getLieuRetrait().toString()}</p>
                            <p>Vendeur: ${enchere.getArticle().getUtilisateur().getPseudo()}</p>
                            <p>
	                               <label for="montantEnchere">Ma proposition :</label>
									<input type="number" size="4" id="montantEnchere" name="montantEnchere" min="${enchere.getMontant()+1}" placeholder="${enchere.getMontant()+1}">
									<input type="hidden" id="idArticle" name="idArticle" value="${enchere.getArticle().getId()}">
									<c:if test="${displayEncherir ==1 }">							
									 <input  class="btn btn-primary btn-lg" style="margin-left:30px" type="submit" value="Enchérir">
									 </c:if>
                            </p>
                        </div>
                       

                    </div>

                </div>
            </div>

</form>


</section>
</div>
<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
		</body>

		</html>