<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="/WEB-INF/includes/navbar.jsp"></jsp:include>
<div class="container">
	<!-- Jumbotron Header-->
	<section class="jumbotron my-4">
		<h1 class="row justify-content-center">Profil de
			${utilisateur.pseudo}</h1>
		<br> <br>

		<div class="row">
			<div class="col">
				<div class="row justify-content-center">
					<div class="col-4">
						<h5>Nom : ${utilisateur.nom}</h5>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<div class="row justify-content-center">
					<div class="col-4">
						<h5>Prénom : ${utilisateur.prenom}</h5>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<div class="row justify-content-center">
					<div class="col-4">
						<h5>Email : ${utilisateur.email}</h5>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<div class="row justify-content-center">
					<div class="col-4">
						<h5>Téléphone : ${utilisateur.telephone}</h5>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<div class="row justify-content-center">
					<div class="col-4">
						<h5>Rue : ${utilisateur.rue}</h5>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<div class="row justify-content-center">
					<div class="col-4">
						<h5>Code postal : ${utilisateur.codePostal}</h5>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<div class="row justify-content-center">
					<div class="col-4">
						<h5>Ville : ${utilisateur.ville}</h5>
					</div>
				</div>
			</div>
		</div>
        <br><br>

		<c:set var="u" value="${sessionScope.utilisateur}" />
		<c:if test="${(u != null) && (u.id == utilisateur.id)}">
			<div class="row">
				<div class="col-lg-6 col-md-10 mx-auto">
					<a class="btn btn-primary btn-lg btn-block"
						data-cke-saved-href="modifierProfil" role="button"
						href="modifierProfil" role="button">Modifier mon profil</a>
						<br>
				</div>
				<div class="col-lg-6 col-md-10 mx-auto">
					<a class="btn btn-warning btn-lg btn-block"
						data-cke-saved-href="modifierMotDePasse" role="button"
						href="modifierMotDePasse" role="button">Modifier mon mot de passe</a>
				</div>
			</div>
		</c:if>

	</section>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
</body>
</html>