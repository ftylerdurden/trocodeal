<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="/WEB-INF/includes/navbar.jsp"></jsp:include>



<div class="container">
	<!-- Jumbotron Header-->
	<section class="jumbotron my-4">
		<h1 class="row justify-content-center">Inscription</h1>
		<br> <br>

		<div class="row justify-content-center">
			<div class="row">
				<c:if test="${!empty StringListeCodesErreurs}">

					<div class="col-12 row justify-content-center">
						<c:forEach var="l" items="${StringListeCodesErreurs}">
							<div style="color: red; font-size: 1.5em;"
								class="col-12 text-center">
								<p>${l}</p>
							</div>
						</c:forEach>
					</div>
				</c:if>
			</div>
		</div>
		<br> <br>

		<form action="${pageContext.servletContext.contextPath}/inscription"
			method="post">
			<div class="row">
				<div class="col">
					<div class="row justify-content-start">
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Pseudo : </label> <input class="form-control" type="text"
								name="pseudo" value="${utilisateur.pseudo}" required="required"
								placeholder="ex. JaneDoe23"><br>
						</div>
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Nom : </label> <input class="form-control" type="text"
								name="nom" value="${utilisateur.nom}" required="required"
								placeholder="ex. Doe"><br>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col">
					<div class="row justify-content-start">
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Prénom : </label> <input class="form-control" type="text"
								name="prenom" value="${utilisateur.prenom}" required="required"
								placeholder="ex. Jane"><br>
						</div>
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Email : </label> <input class="form-control" type="email"
								name="email" value="${utilisateur.email}" required="required"
								placeholder="ex. jane.doe@lost.gone "><br>

						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col">
					<div class="row justify-content-start">
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Téléphone : </label> <input class="form-control"
								type="text" name="telephone" value="${utilisateur.telephone}"
								required="required" placeholder="ex. 06.69.33.69.33"><br>
						</div>
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Rue : </label> <input class="form-control" type="text"
								name="rue" value="${utilisateur.rue}" required="required"
								placeholder="ex. 4 rue des crocodiles"><br>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col">
					<div class="row justify-content-start">
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Code postal : </label> <input class="form-control"
								type="text" name="codePostal" value="${utilisateur.codePostal}"
								required="required" placeholder="ex. 35000"><br>
						</div>
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Ville : </label> <input class="form-control" type="text"
								name="ville" value="${utilisateur.ville}" required="required"
								placeholder="ex. Potts County"><br>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col">
					<div class="row justify-content-start">
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Mot de passe : </label> <input class="form-control"
								type="password" name="motDePasse"
								value="${utilisateur.motDePasse}" required="required"
								placeholder="8 caract. mini, minuscules, majuscules, chiffres, signes spé. (@ . # ! $ % & + =)"><br>
						</div>
						<div class="col-lg-6 col-md-6 mb-12">
							<label>Vérification du mot de passe :</label> <input
								class="form-control" type="password" name="confirmation"
								required="required"
								placeholder="8 caract. mini, minuscules, majuscules, chiffres, signes spé. (@ . # ! $ % & + =)"><br>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<div class="row">
				<div class="col-lg-4 col-md-10 mx-auto">
					<input class="btn btn-primary btn-lg btn-block" type="submit"
						value="Enregistrer"><br>
				</div>
				<div class="col-lg-4 col-md-10 mx-auto">
					<a class="btn btn-secondary btn-lg btn-block"
						data-cke-saved-href="annuler" role="button"
						href="${pageContext.servletContext.contextPath}/">Annuler</a>
				</div>
			</div>

		</form>
	</section>
</div>
<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
</body>
</html>
