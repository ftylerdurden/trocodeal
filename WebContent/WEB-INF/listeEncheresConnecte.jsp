<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>

<jsp:include page="/WEB-INF/includes/navbar.jsp"></jsp:include>
<div class="container">
	<section class="jumbotron my-4 justify-content-center">
		<h1 class="nav justify-content-center" style="margin: 20px 20px">Liste
			des enchères</h1>
		<br>
		<section class="justify-content-center">
			<div id="recherche-div">
				<!-- 			<h2>Filtres</h2> -->
				<form action="" method="post" class="rechercheForm">
					<!-- Filtres-->
					<div class="form-row justify-content-center">
						<div class="col-6">
							<!-- Recherche-->
							<div class="row">
								<div class="col">
									<input class="form-control" type="text" name="recherche"
										id="recherche" placeholder="Le nom de l'article contient">
								</div>
							</div>
							<!-- Categories -->
							<div class="row">
								<div class="col">
									<select class="form-control" name="categorie" id="cat-select">
										<option value="">Toutes</option>
										<c:forEach var="categorie" items="${categories}">
											<option value="${categorie.getId()}">${categorie.getLibelle()}
											</option>
										</c:forEach>
									</select>
								</div>
							</div>

							<br>
							<!-- Filtre achat / ventes -->
							<div class="row">

								<!-- Achats -->
								<div class="col-6">
									<input class="form-check-input" type="radio" id="achats"
										name="achatVente" checked="checked"> <label
										for="achats">Achats</label>

									<div class="achats">
										<div>
											<input class="form-check-input" type="radio"
												id="encheresOuvertes" name="enchereFiltre"
												onclick="location.href = '${pageContext.request.contextPath}/accueilEncheres?filtreEnchere=1';">
											<label for="encheresOuvertes">enchères ouvertes</label>
										</div>
										<div>
											<input class="form-check-input" type="radio" id="mesEncheres"
												name="enchereFiltre"
												onclick="location.href = '${pageContext.request.contextPath}/accueilEncheres?filtreEnchere=2';">
											<label for="mesEncheres">mes enchères</label>
										</div>
										<div>
											<input class="form-check-input" type="radio"
												id="encheresRemportees" name="enchereFiltre"
												onclick="location.href = '${pageContext.request.contextPath}/accueilEncheres?filtreEnchere=3';">
											<label for="encheresRemportees">mes enchères
												remportées</label>
										</div>
									</div>
								</div>
								<!-- Ventes -->
								<div class="col-6">
									<input class="form-check-input" type="radio" id="mesVentes"
										name="achatVente"
										onclick="location.href = '${pageContext.request.contextPath}/listeEncheresMesVentes';">
									<label for="mesVentes">Mes ventes</label>

									<div class="mes-ventes">
										<div>
											<input class="form-check-input" type="radio"
												id="encheresOuvertes" name="venteFiltre" disabled> <label
												for="encheresOuvertes">mes ventes en cours</label>
										</div>
										<div>
											<input class="form-check-input" type="radio" id="mesEncheres"
												name="venteFiltre" disabled> <label
												for="mesEncheres">ventes non débutées</label>
										</div>
										<div>
											<input class="form-check-input" type="radio"
												id="encheresRemportees" name="venteFiltre" disabled>
											<label for="encheresRemportees">ventes terminées</label>
										</div>
									</div>
								</div>

								<!-- Bouton rechercher-->
								<div class="col-12 justify-content-center">
									<input class="btn btn-primary" type="submit" value="Rechercher">
								</div>
							</div>
						</div>
					</div>
					<div></div>
				</form>
			</div>
		</section>
		<br><hr><br>
		<!--Affichage des articles-->
		<section>
			<div class="container col-12">
				<!--Affichage des articles-->
				<ul class="nav justify-content-start">
					<c:forEach var="articles" items="${ventesEncours}">
						<li class="col-lg-4 col-md-6 mb-12" style="margin-bottom: 10px">
							<div class="row text-center col-lg-12">
								<div class="col-lg-12">
									<div class="card h-100">
										<!-- 									<img class="card-img-top" -->
										<!-- 										src="images/image2.png" alt="..."/> -->
										<div class="card-body">
											<h2>
												<a href="${pageContext.request.contextPath}/encherir?idArticle=${articles.getId()}">${articles.getNom()}</a>
											</h2>
											<p class="card-text">
												Début de l'enchère :<br>
												<javatime:format value="${articles.getDateDebutEncheres()}"
													style="MS" />
											</p>
											<p class="card-text">
												Fin de l'enchère :<br>
												<javatime:format value="${articles.getDateFinEncheres()}"
													style="MS" />
											</p>
											<p class="card-text">
												par : <a
													href="${pageContext.servletContext.contextPath}/profil?id=${articles.utilisateur.getId()}">
													${articles.utilisateur.getPseudo()} </a>
											</p>
											<p class="card-text">${articles.getDescription()}</p>
										</div>
										<div class="card-footer">
											<a class="btn btn-primary" href="${pageContext.request.contextPath}/encherir?idArticle=${articles.getId()}">
												${articles.getMiseAPrix()} crédits</a>
										</div>
									</div>
								</div>
							</div>
						<li>
					</c:forEach>
				</ul>
			</div>

		</section>


	</section>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
</body>

</html>