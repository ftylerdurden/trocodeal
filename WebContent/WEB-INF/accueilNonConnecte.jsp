<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>

<jsp:include page="/WEB-INF/includes/navbar.jsp"></jsp:include>
<div class="container">
<section class="jumbotron my-4 justify-content-center">
	<section class="justify-content-center">
		<h1 class="nav justify-content-center" style="margin:20px 20px">Liste des enchères</h1>
		<div id="recherche-div">
<!-- 			<h2>Filtres</h2> -->
			<form action="${pageContext.servletContext.contextPath}/index"
				method="post" class="rechercheForm">
				<div class="form-row justify-content-center">
					<div class="col-6">
						<div class="row">
							<div class="col">
								<input class="form-control" type="text" name="recherche" id="recherche"
									placeholder="Le nom de l'article contient">
							</div>
						</div>
						<div class="row">
							<div class="col">
								<select class="form-control" name="categorie" id="cat-select">
									<option value="">Toutes</option>
									<c:forEach var="categorie" items="${categories}">
										<option value="${categorie.getId()}">${categorie.getLibelle()}
										</option>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>
					<div class="col">
						<input class="btn btn-primary" type="submit" value="Rechercher">
					</div>
				</div>
			</form>
		</div>
	</section>

	<br>
	<hr>
	<br>
	<section>
		<div class="container col-12">
			<!--Affichage des articles-->
			<ul class="nav justify-content-start">
				<c:forEach var="article" items="${listeArticles}">
					<li class="col-lg-4 col-md-6 mb-12" style="margin-bottom:10px">
						<div class="row text-center col-lg-12">
							<div class="col-lg-12">
								<div class="card h-100">
<!-- 									<img class="card-img-top" -->
<!-- 										src="images/image2.png" alt="..."/> -->
									<div class="card-body">
										<h4 class="card-title">${article.getNom()}</h4>
										<p class="card-text">Début de l'enchère :<br>
											 <javatime:format  value="${article.getDateDebutEncheres()}" style="MS" /></p>
										<p class="card-text">Fin de l'enchère :<br>
											 <javatime:format value="${article.getDateFinEncheres()}" style="MS" /></p>
											 <c:if test="${!empty sessionScope.utilisateur }">
											<p class="card-text">par : <a href="${pageContext.servletContext.contextPath}/profil?id=${article.utilisateur.getId()}">  ${article.utilisateur.getPseudo()} </a></p>
											</c:if>
											 <c:if test="${empty sessionScope.utilisateur }">
											<p class="card-text">par : ${article.utilisateur.getPseudo()}</p>
											</c:if>
											<p class="card-text">${article.getDescription()}</p>
									</div>

									<div class="card-footer">
									<c:if test="${!empty sessionScope.utilisateur }">
										<a class="btn btn-success" href="${pageContext.servletContext.contextPath}/encherir?idArticle=${article.getId()}">${article.getMiseAPrix()} crédits</a>
									</c:if>
									<c:if test="${empty sessionScope.utilisateur }">
										<a class="btn btn-light"  >${article.getMiseAPrix()} crédits</a>
									</c:if>
									</div>
								</div>
							</div>
						</div>
					<li>
				</c:forEach>
			</ul>
		</div>

	</section>
	</section>
</div>
<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
</body>

</html>