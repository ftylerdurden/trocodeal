<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page session="true"%>

<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta name="description" content="TrocODeal - ENI">
<meta name="author"
	content="Laura DUFAUT, Claude LUSSEAU, Fabien BIGUET, David HUARD">
<title>TrocODeal</title>
<!-- Favicon-->
<link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
<!-- Bootstrap core CSS -->
<link href="vendor/css/styles.css" rel="stylesheet">

</head>