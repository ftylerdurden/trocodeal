<!-- Navigation -->
<jsp:include page="/WEB-INF/includes/head.jsp"></jsp:include>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
		<a class="navbar-brand"
			href="${pageContext.servletContext.contextPath}/accueilEncheres"><img
			src="images/LogoTrocoDeal2.png" style="width: 30%; height: 30%"
			alt="TrocODeal - Ventes aux enchères"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarResponsive" aria-controls="navbarResponsive"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<c:if test="${!empty sessionScope.utilisateur}">
					<li class="nav-item "><a class="nav-link"
						href="${pageContext.servletContext.contextPath}/accueilEncheres">Encheres</a></li>
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.servletContext.contextPath}/vendreArticle">Vendre</a></li>
					<li class="nav-item"><a class="nav-link js-scroll-trigger"
						href="${pageContext.servletContext.contextPath}/profil?id=${utilisateur.id}">Mon
							Profil</a></li>
					<li class="nav-item"><a class="nav-link js-scroll-trigger"
						href="${pageContext.servletContext.contextPath}/deconnexion">Se
							deconnecter</a></li>
				</c:if>
				<c:if test="${empty sessionScope.utilisateur}">
					<li class="nav-item"><a href="#" class="nav-link"
						data-toggle="modal" data-target="#modalTeam">La TrocoTeam</a> <!-- Modal -->
						<div class="modal fade" id="modalTeam" tabindex="-1" role="dialog"
							aria-labelledby="modalTeam" aria-hidden="true">
							<div class="modal-dialog modal-xl" role="document">
								<div class="modal-content col-12">
									<div class="modal-header">
										<h3 class="modal-title" id="exampleModalLabel">La
											TrocoTeam</h3>

										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>

									<div class="modal-body">
									<h5 style="text-align: justify;">Retrouvez le code source de notre projet sur <a href="https://gitlab.com/ftylerdurden/trocodeal" target="_blank">Gitlab</a>.</h5></div>
									<ul class="nav justify-content-start">
										<li class="col-lg-3 col-md-6 mb-6" style="margin-bottom: 10px">
											<div class="row text-center col-lg-12">
												<img class="card-img-top" src="images/team/Laura.jpg"
													alt="Laura" />
												<div class="card-body">
													<h5 class="card-title">Laura DUFAUT</h5>
													<p class="card-text">Conceptrice / développeuse
														d'applications</p>
													<a
														href="https://www.linkedin.com/in/laura-dufaut-17261410b"
														target="_blank" class="btn btn-primary">Linkedin</a>
												</div>
											</div>
										</li>
										<li class="col-lg-3 col-md-6 mb-6" style="margin-bottom: 10px">
											<div class="row text-center col-lg-12">
												<img class="card-img-top" src="images/team/Fabien.jpg"
													alt="Laura" />
												<div class="card-body">
													<h5 class="card-title">Fabien BIGUET</h5>
													<p class="card-text">Concepteur / développeur
														d'applications</p>
													<a
														href="https://www.linkedin.com/in/fabien-biguet-590340107"
														target="_blank" class="btn btn-primary">Linkedin</a>
												</div>
											</div>
										</li>
										<li class="col-lg-3 col-md-6 mb-6" style="margin-bottom: 10px">
											<div class="row text-center col-lg-12">
												<img class="card-img-top" src="images/team/Claude.jpg"
													alt="Laura" />
												<div class="card-body">
													<h5 class="card-title">Claude LUSSEAU</h5>
													<p class="card-text">Concepteur / développeur
														d'applications</p>
													<a href="https://www.linkedin.com/in/claude-lusseau"
														target="_blank" class="btn btn-primary">Linkedin</a> <a
														href="https://claude-lusseau.fr" target="_blank"
														class="btn btn-success">Site web</a>
												</div>
											</div>
										</li>
										<li class="col-lg-3 col-md-6 mb-6" style="margin-bottom: 10px">
											<div class="row text-center col-lg-12">
												<img class="card-img-top" src="images/team/David.jpg"
													alt="Laura" />
												<div class="card-body">
													<h5 class="card-title">David HUARD</h5>
													<p class="card-text">Concepteur / développeur
														d'applications</p>
													<a href="https://www.linkedin.com/in/david-huard-146336179"
														target="_blank" class="btn btn-primary">Linkedin</a>
												</div>
											</div>
										</li>
									</ul>

									<div class="modal-footer">
										<button type="button" class="btn btn-secondary"
											data-dismiss="modal">Fermer</button>
									</div>
								</div>
							</div>
						</div></li>
					<li class="nav-item"><a class="nav-link js-scroll-trigger"
						href="connexion">Connexion</a></li>
				</c:if>

			</ul>
		</div>
	</div>
</nav>

<body>