<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="/WEB-INF/includes/navbar.jsp"></jsp:include>
<div class="container">
    <!-- Jumbotron Header-->
    <section class="jumbotron my-4">
        <h1 class="row justify-content-center">Vous avez remporté la vente</h1>

        <div class="row justify-content-center">
            <div class="row">
                <c:if test="${!empty stringListeCodesErreurs}">

                    <div class="col-12 row justify-content-center">
                        <c:forEach var="l" items="${stringListeCodesErreurs}">
                            <div style="color: red; font-size: 1.5em;" class="col-12 text-center">
                                <p>${l}</p>
                            </div>
                        </c:forEach>
                    </div>
                </c:if>
            </div>
        </div>
        <br> <br>

        <form action="${pageContext.servletContext.contextPath}/encherir" method="post">
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div  class="col-lg-4 col-md- 2 mb-12" >
                            <img width="300px"  src="images/LogoTrocoDeal2.png" style="border:solid 1px #000000">
                        </div>
                        <div class="col-lg-5 col-md-5 mb-12">
                            <h2>${enchere.getArticle().getNom()}</h2>
                            <p>Description : ${enchere.getArticle().getDescription()}</p>
                            <p>Meilleure offre : ${enchere.getMontant()} pts</p>
                            <p>Mise à prix : ${enchere.getArticle().getMiseAPrix()} points</p>
                            <br>
                            <p>Retrait : ${enchere.getArticle().getLieuRetrait().toString()}</p>
                             <br>
                            <p>Vendeur: ${enchere.getArticle().getUtilisateur().getPseudo()}</p>
                            <p>Tel: ${enchere.getArticle().getUtilisateur().getTelephone()}</p>
                            <input  class="btn btn-primary btn-lg" style="margin-top:30px" type="submit" value="Back">
                        
                        
                               
                            </div>
                           
                        </div>

                    </div>

                </div>
           
</form>


</section>
</div>
<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
		</body>

		</html>