<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="/WEB-INF/includes/navbar.jsp"></jsp:include>
<div class="container">
	<!-- Jumbotron Header-->
	<section class="jumbotron my-4">

		<h1 class="row justify-content-center">Connexion</h1>
		<br> <br>

		<div class="row justify-content-center">
			<div class="row">
				<c:if test="${!empty listeCodesErreurs}">

					<div class="col-12 row justify-content-center">
						<c:forEach var="l" items="${listeCodesErreurs}">
							<div style="color: red; font-size: 1.5em;"
								class="col-12 text-center">
								<p>${l}</p>
							</div>
						</c:forEach>
					</div>
				</c:if>
			</div>
		</div>
		<br> <br>

		<section class="row justify-content-center">
			<div class="container-fluid">
				<form action="${pageContext.request.contextPath}/connexion" method="POST" class="form-example">
					<div class="form-group">
						<label for="email">Email: </label> <input name="email"
							type="email" class="form-control" id="email"
							aria-describedby="email" placeholder="Entrez votre adresse eMail"> <small
							id="email" class="form-text text-muted">Nous ne partagerons pas votre adresse eMail.</small>
					</div>
					<div class="form-group">
						<label for="mdp">Mot de passe: </label> <input
							type="password" class="form-control" id="mdp" name="mdp"
							placeholder="Entrez votre mot de passe">
					</div>
					<div class="form-group form-check">
						<input type="checkbox" class="form-check-input" id="keepLogged" name="keepLogged">
						<label class="form-check-label" for="keepLogged">Se souvenir de moi</label><br>
						<a href="${pageContext.request.contextPath}/recupMotDePasse">Mot de passe oublié</a>
					</div>
					
					<br>
					<div class="row">
						<div class="col-lg-4 col-md-10 mx-auto">
							<input class="btn btn-primary btn-lg btn-block" type="submit"
								value="Se connecter"><br>
						</div>
						<div class="col-lg-4 col-md-10 mx-auto">
							<a class="btn btn-secondary btn-lg btn-block"
								data-cke-saved-href="annuler" role="button"
								href="${pageContext.servletContext.contextPath}/">Annuler</a>
						</div>
					</div>
				</form>
				<br>
				<h3 class="col-lg-4 col-md-10 mx-auto" style="text-align: center;">Ou</h3>
				<br>
				<div class="row">
					<div class="col-lg-4 col-md-10 mx-auto">
						<a class="btn btn-success btn-lg btn-block"
							data-cke-saved-href="inscription" role="button"
							href="${pageContext.servletContext.contextPath}/inscription">Creer
							un compte</a>
					</div>
				</div>

			</div>
		</section>
	</section>
</div>
<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
</body>
</html>