package fr.eni.trocodeal.messages;

import java.util.ResourceBundle;

public class LecteurMessage {

	private static ResourceBundle rb;

	static {
		try {
			rb = ResourceBundle.getBundle("fr.eni.trocodeal.messages.messages_erreur");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getMessageErreur(int code) {
		String message = "";

		try {
			if(rb != null) {
				message = rb.getString(String.valueOf(code));
			} else {
				message = "Code erreur inexistant";
			}
		} catch (Exception e) {
			e.printStackTrace();
			message = "Erreur inconnue : " + e.getMessage();
		}

		return message;
	}

}
