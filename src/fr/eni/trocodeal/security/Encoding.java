package fr.eni.trocodeal.security;

import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.codec.binary.Base64;


/**
 * Classe en charge de l'encodage des mots de passe.
 *
 * @author Laura DUFAUT, Fabien BIGUET, Claude LUSSEAU, David HUARD.
 * @version trocodeal - v1.0
 * @date 17 mai 2021
 */
public class Encoding {

	private static final String UNICODE_FORMAT = "UTF8";
	public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
	private KeySpec ks;
	private SecretKeyFactory skf;
	private Cipher cipher;
	byte[] arrayBytes;
	private String myEncryptionKey;
	private String myEncryptionScheme;
	SecretKey key;


	public String encode(String unencodedString) {
		String encodedString = null;
		try {
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plainText = unencodedString.getBytes(UNICODE_FORMAT);
			byte[] ecodedText = cipher.doFinal(plainText);
			encodedString = new String(Base64.encodeBase64(ecodedText));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return encodedString;
	}

	public String decode(String encodedString) {
		String decodedText = null;
		try {
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] ecodedText = Base64.decodeBase64(encodedString);
			byte[] plainText = cipher.doFinal(ecodedText);
			decodedText = new String(plainText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return decodedText;
	}

	public Encoding() throws Exception {
		myEncryptionKey = "ThisIsSpartaThisIsSparta";
		myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;
		arrayBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
		ks = new DESedeKeySpec(arrayBytes);
		skf = SecretKeyFactory.getInstance(myEncryptionScheme);
		cipher = Cipher.getInstance(myEncryptionScheme);
		key = skf.generateSecret(ks);
	}


}
