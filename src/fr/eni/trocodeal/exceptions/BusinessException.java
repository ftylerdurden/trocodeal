/**
 *
 */
package fr.eni.trocodeal.exceptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe en charge de
 *
 * @author Laura DUFAUT, Fabien BIGUET, Claude LUSSEAU, David HUARD.
 * @version trocodeal - v1.0
 * @date 11 mai 2021
 */
public class BusinessException extends Exception {

	private static final long serialVersionUID = 1L;

	private List<Integer> listeCodesErreur;

	/**
	 * Constructeur.
	 */
	public BusinessException() {
		super();
		this.listeCodesErreur = new ArrayList<>();
	}

	/**
	 *
	 * @param code Code de l'erreur. Doit avoir un message associé dans un fichier
	 *             properties.
	 */
	public void ajouterErreur(int code) {
		if (!this.listeCodesErreur.contains(code)) {
			this.listeCodesErreur.add(code);
		}
	}

	/**
	 * @return
	 */
	public boolean hasErreurs() {
		return this.listeCodesErreur.size() > 0;
	}

	/**
	 * @return
	 */
	public List<Integer> getListeCodesErreur() {
		return this.listeCodesErreur;
	}

}