/**
 *
 */
package fr.eni.trocodeal.exceptions;

/**
 * Classe en charge de
 *
 * @author Laura DUFAUT, Fabien BIGUET, Claude LUSSEAU, David HUARD.
 * @version trocodeal - v1.0
 * @date 11 mai 2021
 */
public class DALException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructeur.
	 */
	public DALException() {
		super();
	}

	/**
	 * Constructeur.
	 *
	 * @param message
	 */
	public DALException(String message) {
		super(message);
	}

	/**
	 * Constructeur.
	 *
	 * @param message
	 * @param exception
	 */
	public DALException(String message, Throwable exception) {
		super(message, exception);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getMessage() {
		StringBuffer sb = new StringBuffer("Couche DAL - ");
		sb.append(super.getMessage());

		return sb.toString();
	}

}
