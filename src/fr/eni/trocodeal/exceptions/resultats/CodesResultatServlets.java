package fr.eni.trocodeal.exceptions.resultats;

/**
 * Classe en charge de
 *
 * @author Laura DUFAUT, Fabien BIGUET, Claude LUSSEAU, David HUARD.
 * @version trocodeal - v1.0
 * @date 13 mai 2021
 */
public class CodesResultatServlets {

	/*
	 * Code Resultat inscription
	 */

	public static final Integer ERREUR_PSEUDO_1 = 30000; // Veuillez renseigner votre pseudonyme.
	public static final Integer ERREUR_PSEUDO_2 = 30001; // Ce pseudonyme est déjà utilisé.
	public static final Integer NON_UTILISE = 30002;
	public static final Integer ERREUR_NOM = 30003; // Veuillez renseigner votre nom.
	public static final Integer ERREUR_PRENOM = 30004; // Veuillez renseigner votre prénom.
	public static final Integer ERREUR_MAIL = 30005; // Veuillez saisir votre adresse mail.
	public static final Integer ERREUR_MAIL_2 = 30006; // Cette adresse mail est déjà utilisée.
	public static final Integer ERREUR_TEL = 30007; // Veuillez renseigner votre numero de téléphone.
	public static final Integer ERREUR_RUE = 30008; // Veuillez renseigner votre adresse (numero, rue, etc.).
	public static final Integer ERREUR_VILLE = 30009; // Veuillez renseigner votre numero de ville.
	public static final Integer ERREUR_CP = 30010; // Veuillez renseigner votre code postal.
	public static final Integer ERREUR_MDP = 30011; // Veuillez choisir un mot de passe.
	public static final Integer ERREUR_ANCIEN_MDP = 30012; // Vous n'avez pas entré votre mot de passe actuel.
	public static final Integer ERREUR_MDP3 = 30013; // le mot de passe et la vérification ne correspondent pas.
	public static final Integer ERREUR_CARACTERES = 30020; // Merci de saisir uniquement des caractères valides (max
															// 30).
	public static final Integer ERREUR_CARACTERES_MDP = 30021; // Veuillez saisir un mot de passe de plus de huit
																// caractères avec minuscules, majuscules, chiffres et
																// signes spéciaux (@ . # ! $ % & + =).
	public static final Integer ERREUR_CARACTERES_MAIL = 30022; // Veuillez saisir une adresse mail valide
																// (truc.machin@chose.toc).
	public static final Integer ERREUR_CARACTERES_PSEUDO = 30023; // Merci de saisir uniquement des caractères valides
																	// (max 30).
	public static final Integer ERREUR_CARACTERES_NOM = 30024; // Merci de saisir uniquement des caractères valides (max
																// 30).
	public static final Integer ERREUR_CARACTERES_PRENOM = 30025; // Merci de respecter le format valides (ex:
																	// xx.xx.xx.xx.xx).
	public static final Integer ERREUR_CARACTERES_TELEPHONE = 30026; // Merci de saisir uniquement des caractères
																		// valides (max 30).
	public static final Integer ERREUR_CARACTERES_RUE = 30027; // Merci de saisir uniquement des caractères valides (max
																// 30).
	public static final Integer ERREUR_CARACTERES_VILLE = 30028; // Merci de saisir uniquement des caractères valides
																	// (max 30).
	public static final Integer ERREUR_CARACTERES_CODEPOSTAL = 30029; // Merci de saisir uniquement des caractères
																		// valides (max 5).
	public static final Integer ERREUR_MDP_VALIDE_CHANGE = 30030; // merci de saisir votre mot de passe actuel pour valider le nouveau mot de passe.
	public static final Integer ERREUR_MDP_OLD_CHANGE = 30031; // Le nouveau mot de passe doit être different de l'ancien.
	public static final Integer ERREUR_MDP_OLD = 30032; // Erreur lors de la saisie de votre mot de passe actuel.

	/*
	 * Code Resultat ArticleVendus
	 */
	public static final Integer FORMAT_NOM_ARTICLE_ERREUR = 40000;
	public static final Integer FORMAT_DESCRIPTION_ERREUR = 40001;
	public static final Integer ERREUR_FORMAT_MISE_A_PRIX = 40002;
	public static final Integer ERREUR_CATEGORIE = 40003;

	/**
	 * Code Encheres
	 */
	public static final Integer ERREUR_CHOISIR_UN_MONTANT = 50000;
	public static final Integer ERREUR_CREDIT_INSUFFISANT = 50001;



}
