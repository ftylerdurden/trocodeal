package fr.eni.trocodeal.exceptions.resultats;

/**
 * Classe en charge du retour des codes disponibles entre 10000 et 19999.
 *
 * @author Laura DUFAUT, Fabien BIGUET, Claude LUSSEAU, David HUARD.
 * @version trocodeal - v1.0
 * @date 11 mai 2021
 */
public abstract class CodesResultatDAL {

	/**
	 * Code Resultat Utilisateur
	 */
	public static final int INSERT_UTILISATEUR_NULL = 10040;
	public static final int INSERT_UTILISATEUR_ECHEC = 10041;
	public static final int UPDATE_UTILISATEUR_NULL = 10042;
	public static final int UPDATE_UTILISATEUR_ECHEC = 10043;
	public static final int DELETE_UTILISATEUR_ECHEC = 10044;
	public static final int DELETE_UTILISATEUR_NULL = 10045;
	public static final int LECTURE_UTILISATEUR_ECHEC = 10046;
	public static final int LECTURE_UTILISATEUR_BY_FIRST_NAME_NULL = 10047;
	public static final int LECTURE_UTILISATEUR_BY_FIRST_NAME_ECHEC = 10048;
	public static final int LECTURE_UTILISATEUR_BY_ID_NULL = 10049;
	public static final int LECTURE_UTILISATEUR_BY_ID_ECHEC = 10050;
	public static final int LECTURE_UTILISATEUR_BY_MAIL_NULL = 10053;
	public static final int CONNEXION_UTILISATEUR_NULL = 10060;
	public static final int CONNEXION_UTILISATEUR_ECHEC = 10061;

	/**
	 * Code Résultat Articles Vendus
	 */
	public static final int DELETE_ARTICLES_VENDUS_ECHEC = 10_100;

}
