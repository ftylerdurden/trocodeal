package fr.eni.trocodeal.dal;

public abstract class CodesErreurDAL {

	public static final int BDD_ERREUR = 10_000;
	public static final int DELETE_ARTICLES_VENDUS_NULL = 10_001;
	public static final int INSERT_ELEMENT_INNEXISTANT = 10_002;

}
