/**
 * 
 */
package fr.eni.trocodeal.dal;

import java.util.List;

import fr.eni.trocodeal.bo.ArticleVendu;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Classe en charge
 * 
 * @author Laura
 * @version trocodeal - v1.0
 * @date 11 mai 2021 - 15:41:56
 */
public interface ArticleVenduDao {

	/**
	 * Méthode en charge de selectionner tous les articles vendus.
	 *
	 * @return List<ArticleVendu>
	 * @throws BusinessException
	 */
	List<ArticleVendu> selectAll() throws BusinessException;

	/**
	 * @param articleVendu
	 * @return
	 * @throws BusinessException
	 */
	ArticleVendu insert(ArticleVendu articleVendu) throws BusinessException;

	/**
	 * @param id
	 * @throws BusinessException
	 */
	void delete(int id) throws BusinessException;

	/**
	 * @param recherche
	 * @param categorie
	 * @return
	 * @throws BusinessException
	 */
	List<ArticleVendu> searchArticle(String recherche, String categorie) throws BusinessException;

	/**
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	public ArticleVendu selectById(int id) throws BusinessException;

	List<ArticleVendu> selectAllOpen() throws BusinessException;

	List<ArticleVendu> listerMesVentesEnCours(int id) throws BusinessException;

	List<ArticleVendu> listerMesVentesNonDebutess(int id) throws BusinessException;

	List<ArticleVendu> listerMesVentesTerminees(int id) throws BusinessException;

/**
	 * @param articleVendu
	 * @throws BusinessException
	 */
	ArticleVendu update(ArticleVendu articleVendu) throws BusinessException;;
}
