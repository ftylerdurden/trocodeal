package fr.eni.trocodeal.dal.test;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.dal.impl.UtilisateurDAOJdbcImpl;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Servlet implementation class SerlvetTestDal
 */
@WebServlet("/SerlvetTestDal")
public class SerlvetTestDal extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static UtilisateurDAOJdbcImpl dao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SerlvetTestDal() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			dao.delete(176);
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
