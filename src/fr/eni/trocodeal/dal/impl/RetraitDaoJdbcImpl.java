package fr.eni.trocodeal.dal.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import fr.eni.trocodeal.bo.ArticleVendu;
import fr.eni.trocodeal.bo.Categorie;
import fr.eni.trocodeal.bo.Retrait;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.dal.CodesErreurDAL;
import fr.eni.trocodeal.dal.RetraitDAO;
import fr.eni.trocodeal.exceptions.BusinessException;

public class RetraitDaoJdbcImpl implements RetraitDAO {

	private static final String SELECT_ALL_RETRAITS = "SELECT * FROM RETRAITS r "
			+ "LEFT JOIN ARTICLES_VENDUS a ON a.no_article = r.no_article "
			+ "LEFT JOIN UTILISATEURS u ON u.no_utilisateur = a.no_utilisateur "
			+ "LEFT JOIN CATEGORIES c ON c.no_categorie = a.no_categorie";

	@Override
	public List<Retrait> selectAll() throws BusinessException {
		List<Retrait> resultat = new ArrayList<Retrait>();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			Statement stmt = cnx.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_ALL_RETRAITS);
			while (rs.next()) {
				// On récupère les info pour retrait sans article
				Retrait retrait = new Retrait();
				retrait.setRue(rs.getString(2));
				retrait.setCodePostal(rs.getString(3));
				retrait.setVille(rs.getString(4));

				// on récupère les infos pour article
				ArticleVendu article = new ArticleVendu();
				article.setId(rs.getInt(5));
				article.setNom(rs.getString(6));
				article.setDescription(rs.getString(7));
				Date dateDebutEncheresSql = rs.getDate(8);
				LocalDateTime dateDebutEncheresJava = LocalDateTime.ofInstant(dateDebutEncheresSql.toInstant(),
						ZoneId.systemDefault());
				article.setDateDebutEncheres(dateDebutEncheresJava);
				Date dateFinEncheresSql = rs.getDate(9);
				LocalDateTime dateFinEncheresJava = LocalDateTime.ofInstant(dateDebutEncheresSql.toInstant(),
						ZoneId.systemDefault());
				article.setDateFinEncheres(dateFinEncheresJava);
				article.setMiseAPrix(rs.getInt(10));
				article.setPrixVente(rs.getInt(11));

				// on récupère les infos pour utilisateur
				Utilisateur utilisateur = new Utilisateur();
				utilisateur.setId(rs.getInt(14));
				utilisateur.setPseudo(rs.getString(15));
				utilisateur.setNom(rs.getString(16));
				utilisateur.setPrenom(rs.getString(17));
				utilisateur.setEmail(rs.getString(18));
				utilisateur.setTelephone(rs.getString(19));
				utilisateur.setRue(rs.getString(20));
				utilisateur.setCodePostal(rs.getString(21));
				utilisateur.setVille(rs.getString(22));
				utilisateur.setMotDePasse(rs.getString(23));
				utilisateur.setCredit(rs.getInt(24));
				utilisateur.setAdministrateur(rs.getShort(26));

				// catégorie
				Categorie categorie = new Categorie();
				categorie.setId(rs.getInt(26));
				categorie.setLibelle(rs.getString(27));

				// On set catégorie pour article
				article.setCategorie(categorie);
				article.setUtilisateur(utilisateur);
				article.setLieuRetrait(retrait);

				// On article au retrait
				retrait.setArticle(article);

				// on ajoute retrait à la liste de résultat
				resultat.add(retrait);
			}

		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}

		return resultat;
	}

}
