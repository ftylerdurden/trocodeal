package fr.eni.trocodeal.dal.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.dal.UtilisateurDAO;
import fr.eni.trocodeal.exceptions.BusinessException;
import fr.eni.trocodeal.exceptions.resultats.CodesResultatDAL;

/**
 * Classe en charge de
 *
 * @author Laura DUFAUT, Fabien BIGUET, Claude LUSSEAU, David HUARD.
 * @version trocodeal - v1.0
 * @date 11 mai 2021
 */
public class UtilisateurDAOJdbcImpl implements UtilisateurDAO {

	private static final String SLQ_SELECT_ALL_UTILISATEURS = "SELECT no_utilisateur, pseudo, nom, prenom, email, telephone, rue, ville, code_postal, credit, administrateur FROM UTILISATEURS";

	private static final String SQL_SELECT_BY_ID_UTILISATEUR = "SELECT * FROM UTILISATEURS WHERE no_utilisateur = ?";

	private static final String SQL_INSERT_UTILISATEUR = "INSERT INTO UTILISATEURS (pseudo, nom, prenom, email, telephone, rue, ville, code_postal, mot_de_passe, credit, administrateur) VALUES (?,?,?,?,?,?,?,?,?, DEFAULT, 0)";

	private static final String SQL_UPDATE_UTILISATEUR = "UPDATE UTILISATEURS SET pseudo = ?, nom = ?, prenom = ?, email = ?, telephone = ?, rue = ?, ville = ?, code_postal = ? WHERE no_utilisateur = ?";

	private static final String SQL_UPDATE_MOT_DE_PASSE = "UPDATE UTILISATEURS SET mot_de_passe = ? WHERE no_utilisateur = ?";

	private static final String SQL_DELETE_UTILISATEUR = "DELETE FROM UTILISATEURS WHERE no_utilisateur = ?";

	private static final String SQL_LOGIN_UTILISATEUR = "SELECT * FROM UTILISATEURS WHERE email = ? AND mot_de_passe = ?";

	private static final String SQL_VERIF_EMAIL = "SELECT email FROM UTILISATEURS WHERE email = ? LIMIT 1";

	private static final String SQL_VERIF_PSEUDO = "SELECT pseudo FROM UTILISATEURS WHERE pseudo = ? LIMIT 1";

	private static final String SQL_SELECT_BY_EMAIL = "SELECT mot_de_passe FROM UTILISATEURS WHERE email = ?";

	/**
	 * {@inheritDoc}
	 *
	 * @param utilisateur
	 */
	@Override
	public void insert(Utilisateur newUtilisateur) throws BusinessException {
		if (newUtilisateur == null) {
			BusinessException businessException = new BusinessException();
			businessException.ajouterErreur(CodesResultatDAL.INSERT_UTILISATEUR_NULL);
			throw businessException;
		}
		try (Connection cnx = ConnectionProvider.getConnection();
				PreparedStatement pstmt = cnx.prepareStatement(SQL_INSERT_UTILISATEUR,
						Statement.RETURN_GENERATED_KEYS);) {

			mappingInsert(newUtilisateur, pstmt);

			int nbRows = pstmt.executeUpdate();
			if (nbRows == 1) {
				ResultSet rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					newUtilisateur.setId(rs.getInt(1));
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (cnx != null) {
				try {
					cnx.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			BusinessException businessException = new BusinessException();
			if (e.getMessage().contains("no_utilisateur == null")) {
				businessException.ajouterErreur(CodesResultatDAL.INSERT_UTILISATEUR_ECHEC);
			}
			throw businessException;
		}
	}

	/**
	 * @param email
	 * @param motDePasse
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public Utilisateur login(String email, String motDePasse) throws BusinessException {

		Utilisateur utilisateur = null;

		if ((email == null) || (motDePasse == null)) {
			BusinessException businessException = new BusinessException();
			businessException.ajouterErreur(CodesResultatDAL.CONNEXION_UTILISATEUR_NULL);
			throw businessException;
		}
		try (Connection con = ConnectionProvider.getConnection();
				PreparedStatement pstmt = con.prepareStatement(SQL_LOGIN_UTILISATEUR);) {

			pstmt.setString(1, email);
			pstmt.setString(2, motDePasse);
			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {
				utilisateur = mappingLogin(rs);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return utilisateur;
	}

	/**
	 * @param email
	 * @return eMail
	 * @throws BusinessException
	 */
	@Override
	public Boolean verifEmail(String email) throws BusinessException {

		boolean resultat = false;

		try (Connection con = ConnectionProvider.getConnection();
				PreparedStatement pstmt = con.prepareStatement(SQL_VERIF_EMAIL);) {

			pstmt.setString(1, email);
			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {
				resultat = false;
			} else if (!rs.next()) {
				resultat = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultat;
	}

	/**
	 * @param pseudo
	 * @return pseudo
	 * @throws BusinessException
	 */
	@Override
	public Boolean verifPseudo(String pseudo) throws BusinessException {
		boolean resultat = false;

		try (Connection con = ConnectionProvider.getConnection();
				PreparedStatement pstmt = con.prepareStatement(SQL_VERIF_PSEUDO);) {

			pstmt.setString(1, pseudo);
			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {
				resultat = false;
			} else if (!rs.next()) {
				resultat = true;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return resultat;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param id
	 * @return utilisateur
	 */
	@Override
	public Utilisateur selectById(int id) throws BusinessException {
		Utilisateur utilisateur = null;

		if (id == 0) {
			BusinessException businessException = new BusinessException();
			businessException.ajouterErreur(CodesResultatDAL.CONNEXION_UTILISATEUR_NULL);
			throw businessException;
		}
		try (Connection con = ConnectionProvider.getConnection();
				PreparedStatement pstmt = con.prepareStatement(SQL_SELECT_BY_ID_UTILISATEUR);) {

			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {
				utilisateur = mappingSelectById(rs);

			}
		} catch (Exception e) {
			e.printStackTrace();
			BusinessException businessException = new BusinessException();
			if (e.getMessage().contains("no_utilisateur == 0")) {
				businessException.ajouterErreur(CodesResultatDAL.CONNEXION_UTILISATEUR_ECHEC);
			}
		}
		return utilisateur;
	}

	@Override
	public String selectByEmail(String email) throws BusinessException {

		String mdp = null;
		if (email == null) {
			BusinessException businessException = new BusinessException();
			businessException.ajouterErreur(CodesResultatDAL.CONNEXION_UTILISATEUR_NULL);
			throw businessException;
		}
		try (Connection con = ConnectionProvider.getConnection();
				PreparedStatement pstmt = con.prepareStatement(SQL_SELECT_BY_EMAIL);) {

			pstmt.setString(1, email);
			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {
				mdp = rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			BusinessException businessException = new BusinessException();
			if (e.getMessage().contains("no_utilisateur == 0")) {
				businessException.ajouterErreur(CodesResultatDAL.CONNEXION_UTILISATEUR_ECHEC);
			}
		}
		return mdp;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param id
	 */
	@Override
	public void delete(int id) throws BusinessException {
		if (id == 0) {
			BusinessException businessException = new BusinessException();
			businessException.ajouterErreur(CodesResultatDAL.DELETE_UTILISATEUR_NULL);
			throw businessException;
		}
		try (Connection cnx = ConnectionProvider.getConnection();
				PreparedStatement pstmt = cnx.prepareStatement(SQL_DELETE_UTILISATEUR)) {

			pstmt.setInt(1, id);
			pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException businessException = new BusinessException();
			if (e.getMessage().contains("no_utilisateur == 0")) {
				businessException.ajouterErreur(CodesResultatDAL.DELETE_UTILISATEUR_ECHEC);
			}
			throw businessException;
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param updateUtilisateur
	 */
	@Override
	public void update(Utilisateur utilisateur) throws BusinessException {
		if (utilisateur == null || utilisateur.getId() <= 0) {
			throw new BusinessException();
		}
		try (Connection cnx = ConnectionProvider.getConnection();
				PreparedStatement pstmt = cnx.prepareStatement(SQL_UPDATE_UTILISATEUR)) {

			cnx.setAutoCommit(false);
			mappingUpdateUtilisateur(utilisateur, pstmt);

			try {
				pstmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
				cnx.rollback();
				throw new BusinessException();

			}

			cnx.commit();

		} catch (SQLException e) {

			throw new BusinessException();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param updateMdp
	 */
	@Override
	public void updateMdp(Utilisateur utilisateur) throws BusinessException {
		if (utilisateur == null || utilisateur.getId() <= 0) {
			throw new BusinessException();
		}
		try (Connection cnx = ConnectionProvider.getConnection();
				PreparedStatement pstmt = cnx.prepareStatement(SQL_UPDATE_MOT_DE_PASSE)) {

			cnx.setAutoCommit(false);
			pstmt.setString(1, utilisateur.getMotDePasse());
			pstmt.setInt(2, utilisateur.getId());

			try {
				pstmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
				cnx.rollback();
				throw new BusinessException();

			}

			cnx.commit();

		} catch (SQLException e) {

			throw new BusinessException();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @return la liste de tout les utilisateurs.
	 */
	@Override
	public List<Utilisateur> selectAll() throws BusinessException {
		List<Utilisateur> utilisateurs = new ArrayList<>();

		try (Connection cnx = ConnectionProvider.getConnection(); Statement stmt = cnx.createStatement()) {

			ResultSet rs = stmt.executeQuery(SLQ_SELECT_ALL_UTILISATEURS);
			while (rs.next()) {
				mappingSelectAll(utilisateurs, rs);
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (cnx != null) {
				try {
					cnx.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			BusinessException businessException = new BusinessException();
			if (e.getMessage().contains("no_utilisateur == 0")) {
				businessException.ajouterErreur(CodesResultatDAL.LECTURE_UTILISATEUR_ECHEC);
			}
			throw businessException;
		}

		return utilisateurs;
	}

	/**
	 * @param rs
	 * @return utilisateur
	 * @throws SQLException
	 */
	private Utilisateur mappingLogin(ResultSet rs) throws SQLException {
		Utilisateur utilisateur;
		utilisateur = new Utilisateur();
		utilisateur.setId(rs.getInt(1));
		utilisateur.setPseudo(rs.getString(2));
		utilisateur.setNom(rs.getString(3));
		utilisateur.setPrenom(rs.getString(4));
		utilisateur.setEmail(rs.getString(5));
		utilisateur.setTelephone(rs.getString(6));
		utilisateur.setRue(rs.getString(7));
		utilisateur.setCodePostal(rs.getString(8));
		utilisateur.setVille(rs.getString(9));
		utilisateur.setMotDePasse(rs.getString(10));
		utilisateur.setCredit(rs.getInt(11));
		utilisateur.setAdministrateur(rs.getShort(12));
		return utilisateur;
	}

	/**
	 * @param utilisateur
	 * @param pstmt
	 * @throws SQLException
	 */
	private void mappingUpdateUtilisateur(Utilisateur utilisateur, PreparedStatement pstmt) throws SQLException {
		pstmt.setString(1, utilisateur.getPseudo());
		pstmt.setString(2, utilisateur.getNom());
		pstmt.setString(3, utilisateur.getPrenom());
		pstmt.setString(4, utilisateur.getEmail());
		pstmt.setString(5, utilisateur.getTelephone());
		pstmt.setString(6, utilisateur.getRue());
		pstmt.setString(7, utilisateur.getVille());
		pstmt.setString(8, utilisateur.getCodePostal());
		pstmt.setInt(9, utilisateur.getId());
	}

	/**
	 * @param utilisateur
	 * @param pstmt
	 * @throws SQLException
	 */
	private void mappingInsert(Utilisateur newUtilisateur, PreparedStatement pstmt) throws SQLException {
		pstmt.setString(1, newUtilisateur.getPseudo());
		pstmt.setString(2, newUtilisateur.getNom());
		pstmt.setString(3, newUtilisateur.getPrenom());
		pstmt.setString(4, newUtilisateur.getEmail());
		pstmt.setString(5, newUtilisateur.getTelephone());
		pstmt.setString(6, newUtilisateur.getRue());
		pstmt.setString(7, newUtilisateur.getVille());
		pstmt.setString(8, newUtilisateur.getCodePostal());
		pstmt.setString(9, newUtilisateur.getMotDePasse());

	}

	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private Utilisateur mappingSelectById(ResultSet rs) throws SQLException {
		Utilisateur utilisateur;
		utilisateur = new Utilisateur();
		utilisateur.setId(rs.getInt(1));
		utilisateur.setPseudo(rs.getString(2));
		utilisateur.setNom(rs.getString(3));
		utilisateur.setPrenom(rs.getString(4));
		utilisateur.setEmail(rs.getString(5));
		utilisateur.setTelephone(rs.getString(6));
		utilisateur.setRue(rs.getString(7));
		utilisateur.setCodePostal(rs.getString(8));
		utilisateur.setVille(rs.getString(9));
		utilisateur.setMotDePasse(rs.getString(10));
		utilisateur.setCredit(rs.getInt(11));
		utilisateur.setAdministrateur(rs.getShort(12));
		return utilisateur;
	}

	/**
	 * @param utilisateurs
	 * @param rs
	 * @throws SQLException
	 */
	private void mappingSelectAll(List<Utilisateur> utilisateurs, ResultSet rs) throws SQLException {
		Utilisateur utilisateur = new Utilisateur();

		utilisateur.setId(rs.getInt(1));
		utilisateur.setPseudo(rs.getString(2));
		utilisateur.setNom(rs.getString(3));
		utilisateur.setPrenom(rs.getString(4));
		utilisateur.setEmail(rs.getString(5));
		utilisateur.setTelephone(rs.getString(6));
		utilisateur.setRue(rs.getString(7));
		utilisateur.setVille(rs.getString(8));
		utilisateur.setCodePostal(rs.getString(9));
		utilisateur.setCredit(rs.getInt(10));
		utilisateur.setAdministrateur(rs.getShort(11));
		utilisateurs.add(utilisateur);
	}

}
