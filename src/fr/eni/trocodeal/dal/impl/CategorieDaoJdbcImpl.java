package fr.eni.trocodeal.dal.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.eni.trocodeal.bo.Categorie;
import fr.eni.trocodeal.dal.CategorieDAO;
import fr.eni.trocodeal.dal.CodesErreurDAL;
import fr.eni.trocodeal.exceptions.BusinessException;
import fr.eni.trocodeal.exceptions.resultats.CodesResultatDAL;

public class CategorieDaoJdbcImpl implements CategorieDAO {

	private static final String SELECT_ALL_CATEGORIES = "SELECT * FROM CATEGORIES";

	private static final String SELECT_BY_ID = "SELECT * FROM CATEGORIES WHERE no_categorie = ?";

	@Override
	public List<Categorie> selectAll() throws BusinessException {
		List<Categorie> resultat = new ArrayList<Categorie>();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			Statement stmt = cnx.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_ALL_CATEGORIES);
			while (rs.next()) {
				int no_categorie = rs.getInt(1);
				String libelle = rs.getString(2);
				Categorie retrait = new Categorie(no_categorie, libelle);
				resultat.add(retrait);
			}
		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}

		return resultat;
	}

	@Override
	public Categorie selectById(int id) throws BusinessException {
		Categorie categorie = null;
		if (id == 0) {
			BusinessException businessException = new BusinessException();
			businessException.ajouterErreur(CodesResultatDAL.CONNEXION_UTILISATEUR_NULL);
			throw businessException;
		}
		try (Connection con = ConnectionProvider.getConnection();
				PreparedStatement pstmt = con.prepareStatement(SELECT_BY_ID);) {

			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();

			categorie = new Categorie();
			if (rs.next()) {
				categorie.setId(rs.getInt("id"));
				categorie.setLibelle(rs.getString("libelle"));


			}
		} catch (Exception e) {
			e.printStackTrace();
			BusinessException businessException = new BusinessException();
			if (e.getMessage().contains("no_categorie == 0")) {
				businessException.ajouterErreur(CodesResultatDAL.CONNEXION_UTILISATEUR_ECHEC);
			}
		}
		return categorie;
	}


}
