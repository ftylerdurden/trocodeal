/**
 *
 */
package fr.eni.trocodeal.dal.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import fr.eni.trocodeal.bo.ArticleVendu;
import fr.eni.trocodeal.bo.Categorie;
import fr.eni.trocodeal.bo.Retrait;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.dal.ArticleVenduDao;
import fr.eni.trocodeal.dal.CodesErreurDAL;
import fr.eni.trocodeal.exceptions.BusinessException;
import fr.eni.trocodeal.exceptions.resultats.CodesResultatDAL;

/**
 * Classe en charge
 *
 * @author Laura
 * @version trocodeal - v1.0
 * @date 11 mai 2021 - 15:44:53
 */
public class ArticleVenduDaoJdbcImpl implements ArticleVenduDao {
	private final static String SELECT_ALL_ARTICLES_VENDUS = "SELECT * FROM ARTICLES_VENDUS av "
			+ "LEFT JOIN UTILISATEURS u ON av.no_utilisateur = u.no_utilisateur "
			+ "LEFT JOIN CATEGORIES c ON av.no_categorie = c.no_categorie "
			+ "LEFT JOIN RETRAITS r ON r.no_article = av.no_article";
	private final static String INSERT_ARTICLES_VENDUS = "INSERT INTO ARTICLES_VENDUS(nom_article, description, date_debut_encheres, date_fin_encheres, prix_initial, prix_vente, no_utilisateur, no_categorie) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";

	private final static String DELETE_ARTICLES_VENDUS = "DELETE FROM ARTICLES_VENDUS WHERE no_article = ?";
	private static final String SEARCH_ARTICLE = "SELECT * FROM ARTICLES_VENDUS av "
			+ "LEFT JOIN UTILISATEURS u ON av.no_utilisateur = u.no_utilisateur "
			+ "LEFT JOIN CATEGORIES c ON av.no_categorie = c.no_categorie "
			+ "LEFT JOIN RETRAITS r ON r.no_article = av.no_article WHERE av.nom_article LIKE ? AND c.no_categorie LIKE ? ";
	private static final String SELECT_BY_ID = "SELECT * FROM ARTICLES_VENDUS av "
			+ "LEFT JOIN UTILISATEURS u ON av.no_utilisateur = u.no_utilisateur "
			+ "LEFT JOIN CATEGORIES c ON av.no_categorie = c.no_categorie "
			+ "LEFT JOIN RETRAITS r ON r.no_article = av.no_article WHERE av.no_article = ";
	private static final String SELECT_ALL_VENTES_ENCOURS = "SELECT * FROM ARTICLES_VENDUS av "
			+ "LEFT JOIN UTILISATEURS u ON av.no_utilisateur = u.no_utilisateur "
			+ "LEFT JOIN CATEGORIES c ON av.no_categorie = c.no_categorie "
			+ "LEFT JOIN RETRAITS r ON r.no_article = av.no_article WHERE av.date_debut_encheres < CURDATE()  AND av.date_fin_encheres > CURDATE()";
	private static final String SELECT_ALL_VENTES_ENCOURS_UTILISATEUR = "SELECT * FROM ARTICLES_VENDUS av "
			+ "LEFT JOIN UTILISATEURS u ON av.no_utilisateur = u.no_utilisateur "
			+ "LEFT JOIN CATEGORIES c ON av.no_categorie = c.no_categorie "
			+ "LEFT JOIN RETRAITS r ON r.no_article = av.no_article WHERE av.date_debut_encheres < CURDATE()  AND av.date_fin_encheres > CURDATE() AND av.no_utilisateur = ";

	private static final String SELECT_ALL_VENTES_NON_DEBUTEE_UTILISATEUR = "SELECT * FROM ARTICLES_VENDUS av "
			+ "LEFT JOIN UTILISATEURS u ON av.no_utilisateur = u.no_utilisateur "
			+ "LEFT JOIN CATEGORIES c ON av.no_categorie = c.no_categorie "
			+ "LEFT JOIN RETRAITS r ON r.no_article = av.no_article WHERE av.date_debut_encheres > CURDATE()  AND av.no_utilisateur = ";

	private static final String SELECT_ALL_VENTES_TERMINEE_UTILISATEUR = "SELECT * FROM ARTICLES_VENDUS av "
			+ "LEFT JOIN UTILISATEURS u ON av.no_utilisateur = u.no_utilisateur "
			+ "LEFT JOIN CATEGORIES c ON av.no_categorie = c.no_categorie "
			+ "LEFT JOIN RETRAITS r ON r.no_article = av.no_article WHERE av.date_fin_encheres < CURDATE()  AND av.no_utilisateur = ";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ArticleVendu> selectAll() throws BusinessException {
		List<ArticleVendu> listeArticlesVendus = new ArrayList<ArticleVendu>();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			Statement stmt = cnx.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_ALL_ARTICLES_VENDUS);
			while (rs.next()) {
				ArticleVendu articleVendu = mapArticle(rs);
				listeArticlesVendus.add(articleVendu);
			}
		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}
		return listeArticlesVendus;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArticleVendu insert(ArticleVendu articleVendu) throws BusinessException {
		try (Connection cnx = ConnectionProvider.getConnection();
				PreparedStatement pStmt = cnx.prepareStatement(INSERT_ARTICLES_VENDUS,
						Statement.RETURN_GENERATED_KEYS);) {
			// DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd
			// HH:mm.ss");

			cnx.setAutoCommit(false);
			pStmt.setString(1, articleVendu.getNom());
			pStmt.setString(2, articleVendu.getDescription());
			pStmt.setObject(3, (articleVendu.getDateDebutEncheres()));
			pStmt.setObject(4, (articleVendu.getDateFinEncheres()));
			pStmt.setInt(5, articleVendu.getMiseAPrix());
			pStmt.setInt(6, articleVendu.getPrixVente());
			pStmt.setInt(7, articleVendu.getUtilisateur().getId());
			pStmt.setInt(8, articleVendu.getCategorie().getId());
			try {
				pStmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
				cnx.rollback();
			}
			ResultSet rs = pStmt.getGeneratedKeys();
			if (rs.next()) {
				articleVendu.setId(rs.getInt(1));
			}
			cnx.commit();
		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}
		return articleVendu;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(int id) throws BusinessException {

		if (id == 0) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.DELETE_ARTICLES_VENDUS_NULL);
			throw be;
		}

		try (Connection cnx = ConnectionProvider.getConnection();
				PreparedStatement pStmt = cnx.prepareStatement(DELETE_ARTICLES_VENDUS)) {

			pStmt.setInt(1, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			if (e.getMessage().contains("no_article == 0")) {
				be.ajouterErreur(CodesResultatDAL.DELETE_ARTICLES_VENDUS_ECHEC);
			}
			throw be;
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ArticleVendu> searchArticle(String recherche, String idCategorie) throws BusinessException {
		List<ArticleVendu> listeArticlesVendus = new ArrayList<ArticleVendu>();
		try (Connection cnx = ConnectionProvider.getConnection();
				PreparedStatement pstmt = cnx.prepareStatement(SEARCH_ARTICLE);) {
			pstmt.setString(1, "%" + recherche + "%");
			pstmt.setString(2, "%" + idCategorie + "%");
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				ArticleVendu articleVendu = mapArticle(rs);
				listeArticlesVendus.add(articleVendu);
			}
		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}
		return listeArticlesVendus;
	}

	@Override
	public ArticleVendu selectById(int idArticle) throws BusinessException {
		ArticleVendu articleVendu = new ArticleVendu();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			Statement stmt = cnx.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_BY_ID + idArticle);
			if (rs.next()) {
				articleVendu = mapArticle(rs);

			} else {
				articleVendu = null;
			}
		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}
		return articleVendu;
	}

	@Override
	public List<ArticleVendu> selectAllOpen() throws BusinessException {
		List<ArticleVendu> listeArticlesVendus = new ArrayList<ArticleVendu>();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			Statement stmt = cnx.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_ALL_VENTES_ENCOURS);
			while (rs.next()) {
				ArticleVendu articleVendu = mapArticle(rs);
				listeArticlesVendus.add(articleVendu);
			}
		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}
		return listeArticlesVendus;
	}

	@Override
	public List<ArticleVendu> listerMesVentesEnCours(int id) throws BusinessException {
		List<ArticleVendu> listeArticlesVendus = new ArrayList<ArticleVendu>();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			Statement stmt = cnx.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_ALL_VENTES_ENCOURS_UTILISATEUR + id);
			while (rs.next()) {
				ArticleVendu articleVendu = mapArticle(rs);
				listeArticlesVendus.add(articleVendu);
			}
		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}
		return listeArticlesVendus;
	}

	@Override
	public List<ArticleVendu> listerMesVentesNonDebutess(int id) throws BusinessException {
		List<ArticleVendu> listeArticlesVendus = new ArrayList<ArticleVendu>();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			Statement stmt = cnx.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_ALL_VENTES_NON_DEBUTEE_UTILISATEUR + id);
			while (rs.next()) {
				ArticleVendu articleVendu = mapArticle(rs);
				listeArticlesVendus.add(articleVendu);
			}
		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}
		return listeArticlesVendus;
	}

	private ArticleVendu mapArticle(ResultSet rs) throws SQLException {
		ArticleVendu articleVendu = new ArticleVendu();
		Utilisateur utilisateur = new Utilisateur();
		Categorie categorie = new Categorie();
		Retrait retrait = new Retrait();
		articleVendu.setId(rs.getInt(1));
		articleVendu.setNom(rs.getString(2));
		articleVendu.setDescription(rs.getString(3));
		articleVendu.setDateDebutEncheres(LocalDateTime.parse(rs.getTimestamp(4).toLocalDateTime().toString()));
		articleVendu.setDateFinEncheres(LocalDateTime.parse(rs.getTimestamp(5).toLocalDateTime().toString()));
		articleVendu.setMiseAPrix(rs.getInt(6));
		articleVendu.setPrixVente(rs.getInt(7));
		articleVendu.setUtilisateur(utilisateur);
		articleVendu.setCategorie(categorie);
		articleVendu.setLieuRetrait(retrait);
		// on récupère les infos pour utilisateur
		utilisateur.setId(rs.getInt(10));
		utilisateur.setPseudo(rs.getString(11));
		utilisateur.setNom(rs.getString(12));
		utilisateur.setPrenom(rs.getString(13));
		utilisateur.setEmail(rs.getString(14));
		utilisateur.setTelephone(rs.getString(15));
		utilisateur.setRue(rs.getString(16));
		utilisateur.setCodePostal(rs.getString(17));
		utilisateur.setVille(rs.getString(18));
		utilisateur.setMotDePasse(rs.getString(19));
		utilisateur.setCredit(rs.getInt(20));
		utilisateur.setAdministrateur(rs.getShort(21));
		// On récupère la catégorie
		categorie.setId(rs.getInt(22));
		categorie.setLibelle(rs.getString(23));
		// Retrait
		retrait.setRue(rs.getString(25));
		retrait.setCodePostal(rs.getString(26));
		retrait.setVille(rs.getString(27));
		retrait.setArticle(articleVendu);
		return articleVendu;
	}

	@Override
	public List<ArticleVendu> listerMesVentesTerminees(int id) throws BusinessException {
		List<ArticleVendu> listeArticlesVendus = new ArrayList<ArticleVendu>();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			Statement stmt = cnx.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_ALL_VENTES_TERMINEE_UTILISATEUR + id);
			while (rs.next()) {
				ArticleVendu articleVendu = mapArticle(rs);
				listeArticlesVendus.add(articleVendu);
			}
		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}
		return listeArticlesVendus;
	}

	@Override
	public ArticleVendu update(ArticleVendu articleVendu) throws BusinessException {
		try (Connection cnx = ConnectionProvider.getConnection();
				PreparedStatement pStmt = cnx.prepareStatement(INSERT_ARTICLES_VENDUS,
						Statement.RETURN_GENERATED_KEYS);) {
//			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm.ss");

			cnx.setAutoCommit(false);
			pStmt.setString(1, articleVendu.getNom());
			pStmt.setString(2, articleVendu.getDescription());
			pStmt.setObject(3, (articleVendu.getDateDebutEncheres()));
			pStmt.setObject(4, (articleVendu.getDateFinEncheres()));
			pStmt.setInt(5, articleVendu.getMiseAPrix());
			pStmt.setInt(6, articleVendu.getPrixVente());
			pStmt.setInt(7, articleVendu.getUtilisateur().getId());
			pStmt.setInt(8, articleVendu.getCategorie().getId());
			try {
				pStmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
				cnx.rollback();
			}
			ResultSet rs = pStmt.getGeneratedKeys();
			if (rs.next()) {
				articleVendu.setId(rs.getInt(1));
			}
			cnx.commit();
		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}
		return articleVendu;
	}



}