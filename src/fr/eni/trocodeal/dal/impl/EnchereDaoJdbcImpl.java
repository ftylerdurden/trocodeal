package fr.eni.trocodeal.dal.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import fr.eni.trocodeal.bo.ArticleVendu;
import fr.eni.trocodeal.bo.Enchere;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.dal.CodesErreurDAL;
import fr.eni.trocodeal.dal.DaoFactory;
import fr.eni.trocodeal.dal.EnchereDao;
import fr.eni.trocodeal.exceptions.BusinessException;

public class EnchereDaoJdbcImpl implements EnchereDao {

	private static final String SELECT_ALL = "SELECT * FROM ENCHERES e";

	private static final String SELECT_BY_ID_TOP = "SELECT * FROM ENCHERES WHERE no_article = ? ORDER BY montant_enchere DESC LIMIT 1";

	private static final String INSERT = "INSERT INTO ENCHERES(date_enchere, montant_enchere, no_article, no_utilisateur) VALUES (?,?,?,?)";

	private static final String UPDATE = "UPDATE ENCHERES SET no_utilisateur = ? , montant_enchere = ?, date_enchere = ? WHERE no_enchere = ?";

	private static final String SELECT_MES_ENCHERES = "SELECT * FROM ENCHERES  WHERE no_utilisateur =  ";

	private static final String SELECT_REMPORTEES_BY_UTILISATEUR = "SELECT * FROM ENCHERES e "
			+ "LEFT JOIN ARTICLES_VENDUS a ON a.no_article = e.no_article AND a.date_fin_encheres < CURRENT_DATE "
			+ " WHERE e.no_utilisateur = ? ";

	@Override
	public List<Enchere> selectAll() throws BusinessException {

		List<Enchere> listeDesEncheres = new ArrayList<Enchere>();

		try (Connection cnx = ConnectionProvider.getConnection()) {
			Statement stmt = cnx.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_ALL);
			while (rs.next()) {
				Enchere enchere = mappingEnchere(cnx, rs);
				listeDesEncheres.add(enchere);
			}
		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}

		return listeDesEncheres;
	}

	@Override
	public Boolean insert(Enchere nouvelleEnchere) throws BusinessException {
		Boolean resultat = false;
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(INSERT);
			Date dateSql = Date.valueOf(nouvelleEnchere.getDate().toLocalDate());
			pstmt.setDate(1, dateSql);
			pstmt.setInt(2, nouvelleEnchere.getMontant());
			pstmt.setInt(3, nouvelleEnchere.getArticle().getId());
			pstmt.setInt(4, nouvelleEnchere.getUtilisateur().getId());

			int nbLigneAffecter = pstmt.executeUpdate();
			if (nbLigneAffecter == 1) {
				resultat = true;

			}

		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}
		return resultat;
	}

	@Override
	public Enchere selectByIdTop(int idArticle) throws BusinessException {
		Enchere enchere = new Enchere();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_ID_TOP);
			pstmt.setInt(1, idArticle);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				enchere = mappingEnchere(cnx, rs);
			} else {
				enchere = null;
			}
		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}
		return enchere;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Boolean update(Utilisateur utilisateurConnecte, int montantEnchere, LocalDateTime date, int idEnchere) {
		Boolean resultat = false;
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(UPDATE);
			pstmt.setInt(1, utilisateurConnecte.getId());
			pstmt.setInt(2, montantEnchere);
			pstmt.setDate(3, Date.valueOf(date.toLocalDate()));
			pstmt.setInt(4, idEnchere);
			int nbLignesAffectees = pstmt.executeUpdate();
			if (nbLignesAffectees > 0) {
				resultat = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultat;
	}

	@Override
	public List<Enchere> selectMesEncheres(int idUtilisateur) throws BusinessException {

		List<Enchere> listeDesEncheres = new ArrayList<Enchere>();

		try (Connection cnx = ConnectionProvider.getConnection()) {
			Statement stmt = cnx.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_MES_ENCHERES + idUtilisateur);
			while (rs.next()) {
				Enchere enchere = mappingEnchere(cnx, rs);

				listeDesEncheres.add(enchere);
			}
		} catch (SQLException e) {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesErreurDAL.BDD_ERREUR);
			e.printStackTrace();
			throw be;
		}

		return listeDesEncheres;
	}

	private Enchere mappingEnchere(Connection cnx, ResultSet rs) throws SQLException, BusinessException {
		Enchere enchere = new Enchere();
		enchere.setId(rs.getInt(1));
		enchere.setDate(LocalDateTime.parse(rs.getTimestamp(2).toLocalDateTime().toString()));
		enchere.setMontant(rs.getInt(3));

		// on récupère les infos pour utilisateur
		Utilisateur utilisateur = DaoFactory.getInstance().getUtilisateurDAO().selectById(rs.getInt(5));

		// on récupère les infos pour article
		ArticleVendu article = DaoFactory.getInstance().getArticleVenduDao().selectById(rs.getInt(4));

		// On met à jour enchère
		enchere.setUtilisateur(utilisateur);
		enchere.setArticle(article);

		return enchere;
	}

	@Override
	public List<Enchere> selectEncheresEnCoursUtilisateur(int idUtilisateur) throws BusinessException {
		List<Enchere> resultat = new ArrayList<>();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_REMPORTEES_BY_UTILISATEUR);
			pstmt.setInt(1, idUtilisateur);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				Enchere enchere = new Enchere();
				enchere = mappingEnchere(cnx, rs);
				resultat.add(enchere);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultat;
	}

}
