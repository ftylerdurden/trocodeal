package fr.eni.trocodeal.dal;

import fr.eni.trocodeal.dal.impl.ArticleVenduDaoJdbcImpl;
import fr.eni.trocodeal.dal.impl.CategorieDaoJdbcImpl;
import fr.eni.trocodeal.dal.impl.EnchereDaoJdbcImpl;
import fr.eni.trocodeal.dal.impl.RetraitDaoJdbcImpl;
import fr.eni.trocodeal.dal.impl.UtilisateurDAOJdbcImpl;

public class DaoFactory {

	private static DaoFactory instance;

	public static DaoFactory getInstance() {
		if (instance == null) {
			instance = new DaoFactory();
		}
		return instance;
	}

	private DaoFactory() {
	}

	public RetraitDAO getRetraitDAO() {
		return new RetraitDaoJdbcImpl();
	}

	public CategorieDAO getCategorieDAO() {
		return new CategorieDaoJdbcImpl();
	}

	public UtilisateurDAO getUtilisateurDAO() {
		return new UtilisateurDAOJdbcImpl();
	}

	public ArticleVenduDao getArticleVenduDao() {
		return new ArticleVenduDaoJdbcImpl();
	}

	public EnchereDao getEnchereDao() {
		return new EnchereDaoJdbcImpl();
	}

}
