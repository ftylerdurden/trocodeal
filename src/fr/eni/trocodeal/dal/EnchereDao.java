package fr.eni.trocodeal.dal;

import java.time.LocalDateTime;
import java.util.List;

import fr.eni.trocodeal.bo.Enchere;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.exceptions.BusinessException;

public interface EnchereDao {

	Boolean insert(Enchere nouvelleEnchere) throws BusinessException;

	List<Enchere> selectAll() throws BusinessException;

	Enchere selectByIdTop(int idArticle) throws BusinessException;

	boolean delete(int id);

	Boolean update(Utilisateur utilisateurConnecte, int montantEnchere, LocalDateTime date, int idEnchere);

	List<Enchere> selectMesEncheres(int idUtilisateur) throws BusinessException;

	List<Enchere> selectEncheresEnCoursUtilisateur(int idUtilisateur) throws BusinessException;

}
