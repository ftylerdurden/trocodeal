package fr.eni.trocodeal.dal;

import java.util.List;

import fr.eni.trocodeal.bo.Retrait;
import fr.eni.trocodeal.exceptions.BusinessException;

public interface RetraitDAO {

	List<Retrait> selectAll() throws BusinessException;

}
