/**
 *
 */
package fr.eni.trocodeal.dal;

import java.util.List;

import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Classe en charge de
 *
 * @author Laura DUFAUT, Fabien BIGUET, Claude LUSSEAU, David HUARD.
 * @version trocodeal - v1.0
 * @date 11 mai 2021
 */
public interface UtilisateurDAO {

	/**
	 * Methode en charge de lister tout les utilisateurs.
	 *
	 * @return
	 * @throws BusinessException
	 */
	public List<Utilisateur> selectAll() throws BusinessException;

	/**
	 * Methode en charge d'enregistrer un utilisateur en base.
	 *
	 * @param utilisateur
	 * @throws BusinessException
	 */
	public void insert(Utilisateur utilisateur) throws BusinessException;

	/**
	 * Methode en charge de la modification d'un utilisateur en base.
	 *
	 * @param utilisateur
	 * @throws BusinessException
	 */
	public void update(Utilisateur utilisateur) throws BusinessException;

	/**
	 * Methode en charge de la suppression d'un utilisateur de la base.
	 *
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	public void delete(int id) throws BusinessException;

	/**
	 * Methode en charge de selectionner un utilisateur par son numero (id).
	 *
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	public Utilisateur selectById(int id) throws BusinessException;

	/**
	 * @param email
	 * @return
	 * @throws BusinessException
	 */
	public Boolean verifEmail(String email) throws BusinessException;

	/**
	 * @param email
	 * @return
	 * @throws BusinessException
	 */
	public Boolean verifPseudo(String pseudo) throws BusinessException;

	/**
	 *
	 * @param email
	 * @param mdp
	 * @return
	 * @throws BusinessException
	 */
	public Utilisateur login(String email, String mdp) throws BusinessException;

	/**
	 * @param email
	 * @return
	 * @throws BusinessException
	 */
	public String selectByEmail(String email) throws BusinessException;

	/**
	 * @param utilisateur
	 * @throws BusinessException
	 */
	void updateMdp(Utilisateur utilisateur) throws BusinessException;

}
