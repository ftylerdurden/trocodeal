package fr.eni.trocodeal.dal;

import java.util.List;

import fr.eni.trocodeal.bo.Categorie;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Classe en charge de
 * @author Laura DUFAUT, Fabien BIGUET, Claude LUSSEAU, David HUARD.
 * @version trocodeal - v1.0
 * @date 18 mai 2021
 */
public interface CategorieDAO {

	/**
	 * @return List<Categorie>
	 * @throws BusinessException
	 */
	List<Categorie> selectAll() throws BusinessException;

	/**
	 * @param id
	 * @return categorie
	 * @throws BusinessException
	 */
	Categorie selectById(int id) throws BusinessException;

}
