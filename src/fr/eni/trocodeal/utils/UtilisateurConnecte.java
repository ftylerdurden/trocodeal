package fr.eni.trocodeal.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import fr.eni.trocodeal.bll.UtilisateurManager;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.exceptions.BusinessException;

public class UtilisateurConnecte {

	public static Utilisateur getUtilisateur(HttpServletRequest request) {
		Utilisateur utilisateurConnecte = null;
		if (request.getSession().getAttribute("utilisateur") != null) {
			utilisateurConnecte = (Utilisateur) request.getSession().getAttribute("utilisateur");
			System.out.println("utilisateur connecté via session: " + utilisateurConnecte);
		} else {
			Cookie cookiesConnexion = CookieConnexion.getCookieConnexion(request);
			System.out.println("cookiesConnexion: " + cookiesConnexion);
			try {
				int idUtilisateur = Integer.parseInt(cookiesConnexion.getValue());
				utilisateurConnecte = UtilisateurManager.getInstance().selectById(idUtilisateur);
				System.out.println("utilisateur connecté via cookie: " + utilisateurConnecte);
			} catch (NumberFormatException | BusinessException e) {
				e.printStackTrace();
			}
		}
		return utilisateurConnecte;
	}

}
