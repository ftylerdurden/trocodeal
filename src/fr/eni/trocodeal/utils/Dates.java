//package fr.eni.trocodeal.utils;
//
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//
///**
// * Classe en charge de
// * @author Laura DUFAUT, Fabien BIGUET, Claude LUSSEAU, David HUARD.
// * @version trocodeal - v1.0
// * @date 20 mai 2021
// */
//public final class Dates {
//    private Dates() {}
//
//    public static String formatLocalDateTime(LocalDateTime localDateTime, String pattern) {
//        return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
//    }
//}
