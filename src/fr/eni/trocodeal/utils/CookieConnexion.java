package fr.eni.trocodeal.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class CookieConnexion {

	public static Cookie getCookieConnexion(HttpServletRequest httpRequest) {
		Cookie[] cookies = httpRequest.getCookies();
		Cookie cookiesConnexion = null;
		if (cookies != null) {
			for (Cookie c : cookies) {
				if (c.getName().equals("connecte")) {
					cookiesConnexion = c;
					break;
				}
			}
		}
		return cookiesConnexion;
	}

}
