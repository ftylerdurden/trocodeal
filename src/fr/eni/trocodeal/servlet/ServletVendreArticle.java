package fr.eni.trocodeal.servlet;

import java.io.IOException;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.trocodeal.bll.ArticleVenduManager;
import fr.eni.trocodeal.bll.CategorieManager;
import fr.eni.trocodeal.bo.ArticleVendu;
import fr.eni.trocodeal.bo.Categorie;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.exceptions.BusinessException;
import fr.eni.trocodeal.exceptions.resultats.CodesResultatServlets;
import fr.eni.trocodeal.messages.LecteurMessage;

/**
 * Servlet implementation class ServletVandreArticle
 */
@WebServlet("/vendreArticle")
public class ServletVendreArticle extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		listeCategories(request);

		this.getServletContext().getRequestDispatcher("/WEB-INF/vendreArticle.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		String nom = null;
		String description = null;
		Categorie categorie = new Categorie();
		LocalDateTime dateDebutEnchere = null;
		LocalDateTime dateFinEnchere = null;
		int miseAPrix = Integer.parseInt(request.getParameter("miseAPrix"));
		int prixVente = 0;

		List<Integer> listeCodesErreur = new ArrayList<>();
		List<String> listeCodesErreurString = new ArrayList<>();

		try {
			nom = request.getParameter("nom");
		} catch (StringIndexOutOfBoundsException e) {
			listeCodesErreur.add(CodesResultatServlets.FORMAT_NOM_ARTICLE_ERREUR);

		}

		try {
			description = request.getParameter("description");
		} catch (StringIndexOutOfBoundsException e) {
			listeCodesErreur.add(CodesResultatServlets.FORMAT_DESCRIPTION_ERREUR);

		}

		if (request.getParameter("dateDebutEncheres").trim().length() == 0
				&& request.getParameter("dateDebutEncheresTime").trim().length() == 0) {
			dateDebutEnchere = LocalDateTime.now();
		} else {
			String dateDebutEnchereString = request.getParameter("dateDebutEncheres") + "T"
					+ request.getParameter("dateDebutEncheresTime");
			dateDebutEnchere = LocalDateTime.parse(dateDebutEnchereString);
		}

		String dateFinEnchereString = request.getParameter("dateFinEncheres") + "T"
				+ request.getParameter("dateFinEncheresTime");
		dateFinEnchere = LocalDateTime.parse(dateFinEnchereString);

		try {
			if (Integer.parseInt(request.getParameter("miseAPrix")) > 0) {
				miseAPrix = Integer.parseInt(request.getParameter("miseAPrix"));
			} else {
				listeCodesErreur.add(CodesResultatServlets.ERREUR_FORMAT_MISE_A_PRIX);
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			listeCodesErreur.add(CodesResultatServlets.ERREUR_FORMAT_MISE_A_PRIX);
		}

		try {
			if (Integer.parseInt(request.getParameter("categorie")) > 0) {
				recupCategorie(request, categorie);
			} else {
				listeCodesErreur.add(CodesResultatServlets.ERREUR_CATEGORIE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (Integer integer : listeCodesErreur) {
			listeCodesErreurString.add(LecteurMessage.getMessageErreur(integer));
		}

		ArticleVendu articleVendu = new ArticleVendu(nom, description, dateDebutEnchere, dateFinEnchere, miseAPrix,
				prixVente, utilisateur, categorie);

		Clock cl = Clock.systemUTC();
		LocalDateTime now = LocalDateTime.now(cl);
		boolean dateDebut = articleVendu.getDateDebutEncheres().isBefore(now);
		boolean dateFin = articleVendu.getDateDebutEncheres().isAfter(now);

		if (listeCodesErreur.size() > 0) {

			request.setAttribute("article", articleVendu);
			request.setAttribute("listeCodesErreurString", listeCodesErreurString);
			doGet(request, response);

		} else {
			try {
				request.setAttribute("dateDebut", dateDebut);
				request.setAttribute("dateFin", dateFin);
				articleVendu = ArticleVenduManager.getInstance().insert(articleVendu);
				session.setAttribute("noArticle", articleVendu.getId());
				response.sendRedirect(request.getContextPath() + "/");
			} catch (BusinessException e) {
				e.printStackTrace();
				request.setAttribute("listeCodesErreur", e.getListeCodesErreur());
				this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/vendreArticle.jsp").forward(request,
						response);
			}

		}

	}

	/**
	 * @param request
	 * @param categorie
	 */
	private void recupCategorie(HttpServletRequest request, Categorie categorie) {
		int cat = Integer.parseInt(request.getParameter("categorie"));
		categorie.setId(cat);
	}

	/**
	 * @param request
	 */
	private void listeCategories(HttpServletRequest request) {
		List<Categorie> categories = null;

		try {
			categories = CategorieManager.getInstance().selectAll();
		} catch (BusinessException e) {
			e.printStackTrace();
		}

		request.setAttribute("categories", categories);
	}
}