package fr.eni.trocodeal.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.trocodeal.bll.CodeErreurBLL;
import fr.eni.trocodeal.bll.UtilisateurManager;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.exceptions.BusinessException;
import fr.eni.trocodeal.messages.LecteurMessage;
import fr.eni.trocodeal.security.Encoding;

/**
 * Servlet implementation class ServletConnexion
 */
@WebServlet("/connexion")
public class ServletConnexion extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Encoding coding;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/connexion.jsp");
		rd.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			coding = new Encoding();
		} catch (Exception cry) {
			cry.printStackTrace();
		}

		String email = request.getParameter("email");
		String mdp = request.getParameter("mdp");
		String securedMdp = encodePass(mdp);
		String keepLogged = request.getParameter("keepLogged");

		List<Integer> listeCodesErreurs = new ArrayList<>();
		List<String> stringListeCodesErreurs = new ArrayList<>();

		HttpSession session = request.getSession();

		Utilisateur utilisateur = null;
		String bddPass = null;

		try {
			bddPass = bddPass(email);
		} catch (BusinessException e1) {
			e1.printStackTrace();
		}

		try {
			utilisateur = UtilisateurManager.getInstance().connexion(email, securedMdp);
			if ((utilisateur == null) || (!bddPass.equals(securedMdp))) {
				listeCodesErreurs.add(CodeErreurBLL.IDENTIFICATION_ECHOUEE);
				for (int codeErreur : listeCodesErreurs) {
					stringListeCodesErreurs.add(LecteurMessage.getMessageErreur(codeErreur));
				}
				request.setAttribute("listeCodesErreurs", stringListeCodesErreurs);
				session.setAttribute("utilisateur", utilisateur);

				this.getServletContext().getRequestDispatcher("/WEB-INF/connexion.jsp").forward(request, response);
			} else {
				session.setAttribute("utilisateur", utilisateur);
				if ("on".equalsIgnoreCase(keepLogged)) {
					Cookie cookieConnexion = new Cookie("connecte", String.valueOf(utilisateur.getId()));
					response.addCookie(cookieConnexion);
				}
				RequestDispatcher rd = request.getRequestDispatcher("/accueilEncheres");
				rd.forward(request, response);
			}
		} catch (BusinessException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Méthode en charge du chiffrement du mot de passe.
	 *
	 * @param motDePasse
	 * @return securePassword : mot de passe chiffré.
	 */
	private String encodePass(String mdp) {
		String securePassword = coding.encode(mdp);
		return securePassword;
	}

	/**
	 * Méthode en charge du chiffrement du mot de passe.
	 *
	 * @param motDePasse
	 * @return securePassword : mot de passe chiffré.
	 */
	private String bddPass(String email) throws BusinessException {
		String mdp = UtilisateurManager.getInstance().selectByEmail(email);
		return mdp;
	}
}
