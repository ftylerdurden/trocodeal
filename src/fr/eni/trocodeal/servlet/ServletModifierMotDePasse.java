package fr.eni.trocodeal.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.trocodeal.bll.UtilisateurManager;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.exceptions.BusinessException;
import fr.eni.trocodeal.exceptions.resultats.CodesResultatServlets;
import fr.eni.trocodeal.messages.LecteurMessage;
import fr.eni.trocodeal.security.Encoding;

/**
 * Servlet implementation class ServletModifierMotDePasse
 */
@WebServlet("/modifierMotDePasse")
public class ServletModifierMotDePasse extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Encoding code;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletModifierMotDePasse() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		@SuppressWarnings("unused")
		HttpSession session = request.getSession();

		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/modifierMotDePasse.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");

		try {
			code = new Encoding();
		} catch (Exception cry) {
			cry.printStackTrace();
		}
		request.setCharacterEncoding("UTF-8");

		List<Integer> listeCodesErreurs = new ArrayList<>();
		List<String> StringListeCodesErreurs = new ArrayList<>();

		String motDePasse = request.getParameter("motDePasse");
		String nouveauMotDePasse = request.getParameter("nouveauMotDePasse");
		String confirmation = request.getParameter("confirmation");
		String securePassword = encodePassword(nouveauMotDePasse);
		String oldSecurePassword = encodePassword(motDePasse);
		String bddSecurePassword = utilisateur.getMotDePasse();


		verifMotDePasse(listeCodesErreurs, motDePasse, nouveauMotDePasse, confirmation, securePassword,
				oldSecurePassword, bddSecurePassword);


		for (Integer i : listeCodesErreurs) {
			StringListeCodesErreurs.add(LecteurMessage.getMessageErreur(i));
		}

		if (listeCodesErreurs.size() > 0) {
			utilisateur = new Utilisateur(motDePasse);
			request.setAttribute("StringListeCodesErreurs", StringListeCodesErreurs);
			doGet(request, response);
		} else {

			try {
				int id = utilisateur.getId();
				utilisateur = new Utilisateur(id, securePassword);
				UtilisateurManager.getInstance().updateMdp(utilisateur);
				session.setAttribute("utilisateur", utilisateur);
				response.sendRedirect(request.getContextPath() + "/profil?id=" + utilisateur.getId());
			} catch (BusinessException e) {
				e.printStackTrace();
				utilisateur = new Utilisateur();
				request.setAttribute("utilisateur", utilisateur);
				request.setAttribute("StringListeCodesErreurs", StringListeCodesErreurs);
				doGet(request, response);
			}
		}
	}

	/**
	 * Methode en charge de la vérification des MDP saisis (nouveau et ancien).
	 * Le nouveau MDP ne doit pas etre identique à l'ancien.
	 *
	 * @param listeCodesErreurs
	 * @param motDePasse
	 * @param nouveauMotDePasse
	 * @param confirmation
	 * @param securePassword
	 * @param oldSecurePassword
	 * @param bddSecurePassword
	 */
	private void verifMotDePasse(List<Integer> listeCodesErreurs, String motDePasse, String nouveauMotDePasse,
			String confirmation, String securePassword, String oldSecurePassword, String bddSecurePassword) {
		if ((nouveauMotDePasse != null) && (motDePasse == null)) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_MDP_VALIDE_CHANGE);
		} else if (!oldSecurePassword.equals(bddSecurePassword)) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_MDP_OLD);
		} else if (securePassword.equals(bddSecurePassword)) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_MDP_OLD_CHANGE);
		} else if ((nouveauMotDePasse.trim().length() == 0)) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_MDP);
		} else if (!nouveauMotDePasse.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#.!$%^&+=])(?=\\S+$).{8,}$")) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_CARACTERES_MDP);
		} else if (!nouveauMotDePasse.equals(confirmation)) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_MDP3);
		} else if (!nouveauMotDePasse.equals(confirmation)) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_MDP3);
		}
	}

	/**
	 * Méthode en charge de l'encodage du mot de passe.
	 *
	 * @param motDePasse
	 * @return securePassword : mot de passe encodé.
	 */
	private String encodePassword(String motDePasse) {
		String securePassword = motDePasse;
		securePassword = code.encode(motDePasse);
		return securePassword;
	}

}
