package fr.eni.trocodeal.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.trocodeal.bll.UtilisateurManager;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Servlet implementation class ServletProfil
 */
@WebServlet("/profil")
public class ServletProfil extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public ServletProfil() {
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unused")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(true);

		int id = Integer.parseInt(request.getParameter("id")); // Je recupere l'id passé dans l'adresse (http://blabla/profil?id=19)

		Utilisateur utilisateur = null; // Je cree un Objet utilisateur (vide).
		int idUtilisateur = 0;

		if(session.getAttribute("utilisateur") != null) {
			utilisateur = (Utilisateur) session.getAttribute("utilisateur");
			idUtilisateur = utilisateur.getId();
		}

	    try {
	    	// on récupère les attributs d'utilisateur (bdd) grace � son id (passé en parametre)
			utilisateur = UtilisateurManager.getInstance().selectById(id); // on récupère les attributs d'utilisateur (bdd) grace � son id (passé en parametre)
		} catch (BusinessException be) {
			be.printStackTrace();
		}

	    // On met à disposition les attributs pour la jsp.
	    request.setAttribute("utilisateur", utilisateur);

		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/profil.jsp");
		rd.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

