package fr.eni.trocodeal.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.trocodeal.utils.CookieConnexion;

/**
 * Servlet implementation class ServletDeconnexion
 */
@WebServlet("/deconnexion")
public class ServletDeconnexion extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE = "/index";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Récupérer et destruir la session en cours.
		HttpSession session = request.getSession();
		session.invalidate();
		// Vider le cookie
		Cookie cookieConnexion = CookieConnexion.getCookieConnexion(request);
		if (cookieConnexion != null) {
			cookieConnexion.setValue("");
			cookieConnexion.setMaxAge(0);
			response.addCookie(cookieConnexion);
		}

		// Redirection vers la page d'accueil.
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
