package fr.eni.trocodeal.servlet;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.trocodeal.bll.ArticleVenduManager;
import fr.eni.trocodeal.bll.CategorieManager;
import fr.eni.trocodeal.bll.EnchereManager;
import fr.eni.trocodeal.bo.ArticleVendu;
import fr.eni.trocodeal.bo.Categorie;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Servlet implementation class ListeEncheresConnecte
 */
@WebServlet("/accueilEncheres")
public class ServletListeEncheresConnecte extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Utilisateur utilisateurConnecte = (Utilisateur) request.getSession().getAttribute("utilisateur");
		System.out.println("utilisateurConnecte: " + utilisateurConnecte);

		LocalDateTime dateDuJour = LocalDateTime.now();
		List<Categorie> categories = new ArrayList<>();
		List<ArticleVendu> ventesEncours = new ArrayList<>();
		int filtreEnchere = 1;
		if (request.getParameter("filtreEnchere") != null) {
			try {
				filtreEnchere = Integer.valueOf(request.getParameter("filtreEnchere"));
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			}
		}
		switch (filtreEnchere) {
		case 2:
			try {
				ventesEncours = EnchereManager.getInstance().selectMesEncheres(utilisateurConnecte.getId());
			} catch (BusinessException e) {
				e.printStackTrace();
			}
			break;
		case 3:
			try {
				ventesEncours = EnchereManager.getInstance().selectEncheresRemportees(utilisateurConnecte.getId());
			} catch (BusinessException e) {
				e.printStackTrace();
			}
			break;
		default:
			try {
				categories = CategorieManager.getInstance().selectAll();
				ventesEncours = ArticleVenduManager.getInstance().selectAllOpen();
			} catch (BusinessException e) {
				e.printStackTrace();
			}
			break;
		}
		request.setAttribute("categories", categories);
		request.setAttribute("ventesEncours", ventesEncours);
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/listeEncheresConnecte.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Utilisateur utilisateurConnecte = (Utilisateur) request.getSession().getAttribute("utilisateur");
		List<ArticleVendu> resultatRecherche = new ArrayList<>();
		LocalDateTime dateDuJour = LocalDateTime.now();
		String recherche = request.getParameter("recherche");
		String categorie = "";
		Boolean enCours = true;
		if ("".equalsIgnoreCase(recherche) || recherche == null) {
			try {
				resultatRecherche = ArticleVenduManager.getInstance().selectAllOpen();
			} catch (BusinessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {

			categorie = request.getParameter("categorie");
			try {
				resultatRecherche = ArticleVenduManager.getInstance().rechercherArticle(recherche, categorie);
			} catch (BusinessException e) {
				e.printStackTrace();
			}
		}

		request.setAttribute("ventesEncours", resultatRecherche);
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/listeEncheresConnecte.jsp");
		rd.forward(request, response);
	}

}
