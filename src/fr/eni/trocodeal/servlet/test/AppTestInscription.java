package fr.eni.trocodeal.servlet.test;

import fr.eni.trocodeal.dal.DaoFactory;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Classe en charge de
 *
 * @author Laura DUFAUT, Fabien BIGUET, Claude LUSSEAU, David HUARD.
 * @version trocodeal - v1.0
 * @date 16 mai 2021
 */
public class AppTestInscription {

	private static  String email = null;
	private static  String pseudo = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		email = "claude.lusseau@yahoo.fr";
		pseudo = "Kalic";

		System.out.println("Test Boolean");

		if (verificationEmail(email) == false) {
			System.out.println("l'email " + email + " est déjà utilisé");
		} else {
			System.out.println("l'email " + email + " est libre");
		}

		if (verificationPseudo(pseudo) == false) {
			System.out.println("le pseudo " + pseudo + " est déjà utilisé");
		} else {
			System.out.println("le pseudo " + pseudo + " est libre");
		}

	}

	/**
	 * Methode en charge de la vérification de l'existance de l'email.
	 *
	 * @param email
	 * @return
	 */
	private static Boolean verificationEmail(String email) {
		boolean eMail = false;

		try {
			eMail = DaoFactory.getInstance().getUtilisateurDAO().verifEmail(email);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		return eMail;

	}

	/**
	 * Methode en charge de la vérification de l'existance du pseudo.
	 *
	 * @param pseudo
	 * @return
	 */
	private static Boolean verificationPseudo(String pseudo) {
		boolean ps = false;
		try {
			ps = DaoFactory.getInstance().getUtilisateurDAO().verifEmail(pseudo);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		return ps;
	}

}
