package fr.eni.trocodeal.servlet.test;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.trocodeal.bll.UtilisateurManager;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.exceptions.BusinessException;
import fr.eni.trocodeal.security.Encoding;

/**
 * Servlet implementation class ServletTestInscription
 */
@WebServlet("/ServletTestInscription")
public class ServletTestInscription extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

//		String email = "claude.lusseau@ydfs.fr";
//		String pseudo = "harthyn";
//
//		System.out.println("Test Boolean");
//
//		if (!verificationEmail(email)) {
//			System.out.println("l'email " + email + " est déjà utilisé");
//		} else {
//			System.out.println("l'email " + email + " est libre");
//		}
//
//		if (!verificationPseudo(pseudo)) {
//			System.out.println("le pseudo " + pseudo + " est déjà utilisé");
//		} else {
//			System.out.println("le pseudo " + pseudo + " est libre");
//		}

		Encoding decode = null;
		try {
			decode = new Encoding();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Utilisateur utilisateur = null;
		 try {
			utilisateur = UtilisateurManager.getInstance().selectById(237);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		 String mdp = utilisateur.getMotDePasse();
		 System.out.println("mot de pass encodé : " + mdp);
		 String visible = decode.decode(mdp);
		 System.out.println("mot de pass an clair : " + visible);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	/**
	 * Methode en charge de la vérification de l'existance de l'email.
	 *
	 * @param email
	 * @return
	 */
//	private Boolean verificationEmail(String email) {
//		boolean resultat = false;
//		try {
//			resultat = UtilisateurManager.getInstance().verifEmail(email);
//		} catch (BusinessException e) {
//			e.printStackTrace();
//		}
//		return resultat;
//
//	}

	/**
	 * Methode en charge de la vérification de l'existance du pseudo.
	 *
	 * @param pseudo
	 * @return
	 */
//	private Boolean verificationPseudo(String pseudo) {
//		boolean resultat = false;
//		try {
//			resultat = UtilisateurManager.getInstance().verifPseudo(pseudo);
//		} catch (BusinessException e) {
//			e.printStackTrace();
//		}
//		return resultat;
//
//	}

}
