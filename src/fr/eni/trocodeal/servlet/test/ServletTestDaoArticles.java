package fr.eni.trocodeal.servlet.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.trocodeal.bll.ArticleVenduManager;
import fr.eni.trocodeal.bo.ArticleVendu;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Servlet implementation class ServletTestDaoArticles
 */
@WebServlet("/ServletTestDaoArticles")
public class ServletTestDaoArticles extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<ArticleVendu> articleVenduListe = new ArrayList<ArticleVendu>();
		try {
			articleVenduListe = ArticleVenduManager.getInstance().selectAll();
			for (int i = 0; i < 4; i++) {
				ArticleVendu articleVendu = articleVenduListe.get(i);
				System.out.println("ArticleVendu :" + articleVendu);
				System.out.println("Utilisateur: " + articleVendu.getUtilisateur());
				System.out.println("Retrait: " + articleVendu.getLieuRetrait());
				System.out.println("Categorie: " + articleVendu.getCategorie());
			}

			System.out.println("Article");
		} catch (BusinessException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
