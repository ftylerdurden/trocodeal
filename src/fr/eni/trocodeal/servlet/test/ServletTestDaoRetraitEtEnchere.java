package fr.eni.trocodeal.servlet.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.trocodeal.bo.Enchere;
import fr.eni.trocodeal.bo.Retrait;
import fr.eni.trocodeal.dal.DaoFactory;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Servlet implementation class ServletTestDaoRetraitEtEnchere
 */
@WebServlet("/ServletTestDaoRetraitEtEnchere")
public class ServletTestDaoRetraitEtEnchere extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Retrait> retraits = new ArrayList<Retrait>();
		List<Enchere> encheres = new ArrayList<>();
		try {
			System.out.println("----Test Retrait----");
			retraits = DaoFactory.getInstance().getRetraitDAO().selectAll();
			for (int i = 0; i < 4; i++) {
				System.out.println(retraits.get(i));
				System.out.println("Article");
				System.out.println(retraits.get(i).getArticle());
				System.out.println(retraits.get(i).getArticle().getCategorie());
			}
			System.out.println("----Test Enchere----");
			encheres = DaoFactory.getInstance().getEnchereDao().selectAll();
			for (int i = 0; i < 4; i++) {
				System.out.println(encheres.get(i));
				System.out.println("Article");
				System.out.println(encheres.get(i).getArticle());
				System.out.println(encheres.get(i).getUtilisateur());
			}
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
