package fr.eni.trocodeal.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.trocodeal.bll.ArticleVenduManager;
import fr.eni.trocodeal.bll.CategorieManager;
import fr.eni.trocodeal.bo.ArticleVendu;
import fr.eni.trocodeal.bo.Categorie;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Servlet implementation class ServletAccueil
 */
@WebServlet("/index")
public class ServletAccueilNonConnecte extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Categorie> categories = new ArrayList<Categorie>();
		List<ArticleVendu> listeArticles = new ArrayList<>();
		try {
			categories = CategorieManager.getInstance().selectAll();
			listeArticles = ArticleVenduManager.getInstance().selectAll();
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		request.setAttribute("categories", categories);
		request.setAttribute("listeArticles", listeArticles);
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/accueilNonConnecte.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<ArticleVendu> resultatRecherche = new ArrayList<>();
		String recherche = request.getParameter("recherche");
		String categorie = "";
		if ("".equalsIgnoreCase(recherche)) {
			try {
				resultatRecherche = ArticleVenduManager.getInstance().selectAll();
			} catch (BusinessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {

			categorie = request.getParameter("categorie");
			try {
				resultatRecherche = ArticleVenduManager.getInstance().rechercherArticle(recherche, categorie);
			} catch (BusinessException e) {
				e.printStackTrace();
			}
		}

		request.setAttribute("listeArticles", resultatRecherche);
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/accueilNonConnecte.jsp");
		rd.forward(request, response);
	}

}
