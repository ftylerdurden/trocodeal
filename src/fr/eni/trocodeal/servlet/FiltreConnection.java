package fr.eni.trocodeal.servlet;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.trocodeal.bll.UtilisateurManager;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Servlet Filter implementation class FiltreConnection
 */
@WebFilter(dispatcherTypes = { DispatcherType.REQUEST }, urlPatterns = { "/*" })
public class FiltreConnection implements Filter {

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		if (httpRequest.getServletPath().startsWith("/index") || httpRequest.getServletPath().startsWith("/connexion")
				|| httpRequest.getServletPath().startsWith("/vendor") || httpRequest.getServletPath().startsWith("/css")
				|| httpRequest.getServletPath().startsWith("/images")
				|| httpRequest.getServletPath().startsWith("/accueilEncheres")
				|| httpRequest.getServletPath().startsWith("/inscription")
				|| httpRequest.getSession().getAttribute("utilisateur") != null) {
			chain.doFilter(request, response);
			// TODO: rediriger ver accueil non connecte ou connecte suivant (click sur le
			// logo)
		} else {
			Cookie cookiesConnexion = getCookieConnexion(httpRequest);
			if (cookiesConnexion != null) {
				// On relance une session (utile pour redirection via logo dans la navBar)
				try {
					int idUtilisateur = Integer.parseInt(cookiesConnexion.getValue());
					httpRequest.getSession().setAttribute("utilisateur",
							UtilisateurManager.getInstance().selectById(idUtilisateur));
					chain.doFilter(request, response);
				} catch (NumberFormatException | BusinessException e) {
					e.printStackTrace();
				}
			} else {
				httpRequest.setAttribute("urlCible", httpRequest.getContextPath() + httpRequest.getServletPath());
				RequestDispatcher rd = httpRequest.getRequestDispatcher("/WEB-INF/connexion.jsp");
				rd.forward(request, response);
			}
		}
	}

	public Cookie getCookieConnexion(HttpServletRequest httpRequest) {
		Cookie[] cookies = httpRequest.getCookies();
		Cookie cookiesConnexion = null;
		if (cookies != null) {
			for (Cookie c : cookies) {
				if (c.getName().equals("connecte")) {
					cookiesConnexion = c;
					break;
				}
			}
		}
		return cookiesConnexion;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
