package fr.eni.trocodeal.servlet;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.trocodeal.bll.ArticleVenduManager;
import fr.eni.trocodeal.bll.EnchereManager;
import fr.eni.trocodeal.bll.UtilisateurManager;
import fr.eni.trocodeal.bo.ArticleVendu;
import fr.eni.trocodeal.bo.Enchere;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.exceptions.BusinessException;
import fr.eni.trocodeal.exceptions.resultats.CodesResultatServlets;
import fr.eni.trocodeal.messages.LecteurMessage;
import fr.eni.trocodeal.utils.UtilisateurConnecte;

/**
 * Servlet implementation class ServletInscription
 */
@WebServlet("/encherir")
public class ServletEncherir extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Enchere enchere = new Enchere();
		Utilisateur utilisateurConnecte = UtilisateurConnecte.getUtilisateur(request);
		String url = "";
		int idArticle = -1;
		idArticle = getIdArticle(request, idArticle);
		enchere = getEnchere(enchere, idArticle);
		// Si nouvelle enchère
		if (enchere == null) {
			enchere = creerEnchereVide(enchere, idArticle);
			request.setAttribute("displayEncherir", 1);
			url = "/WEB-INF/encherir.jsp";
			// Si surenchère
		} else {
			url = dipatchEnchere(request, enchere, utilisateurConnecte, url, idArticle);
		}
		request.setAttribute("enchere", enchere);
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Utilisateur utilisateurConnecte = (Utilisateur)
		// request.getSession().getAttribute("utilisateur");
		Utilisateur utilisateurConnecte = UtilisateurConnecte.getUtilisateur(request);
		ArticleVendu article = new ArticleVendu();
		String montantEnchereSrting = request.getParameter("montantEnchere");
		int idArticle = -1;
		List<Integer> listeCodesErreurs = new ArrayList<>();
		List<String> stringListeCodesErreurs = new ArrayList<>();
		idArticle = getIdArticle(request, idArticle);
		article = getArticle(article, idArticle);

		if ("".equalsIgnoreCase(montantEnchereSrting)) {
			getListeCodesErreurs(listeCodesErreurs, stringListeCodesErreurs);
			request.setAttribute("stringListeCodesErreurs", stringListeCodesErreurs);
			doGet(request, response);
		} else {
			try {
				int montantEnchere = Integer.valueOf(request.getParameter("montantEnchere"));
				if (utilisateurConnecte.getCredit() < montantEnchere) {
					erreurCreditInsufisant(listeCodesErreurs, stringListeCodesErreurs);
					request.setAttribute("stringListeCodesErreurs", stringListeCodesErreurs);
					doGet(request, response);
				} else {
					Enchere enchereOld = EnchereManager.getInstance().selectByIdTop(idArticle);
					// On créer une nouvelle enchère
					placerEnchere(utilisateurConnecte, article, montantEnchere);
					// Si c'est une surenchère
					if (enchereOld != null) {
						// On met à jour le crédit de l'enchéreur précédent
						Utilisateur enchereurPrecedent = UtilisateurManager.getInstance()
								.selectById(enchereOld.getUtilisateur().getId());
						majCredit(enchereOld, enchereurPrecedent);
					}
					// On récupère l'enchère maj
					// TODO: désactiver bouton enchérir si l'utilisateur est le meilleur enchéreur
					Enchere enchereMaj = EnchereManager.getInstance().selectByIdTop(idArticle);
					response.sendRedirect(request.getContextPath() + "/encherir?idArticle="
							+ enchereMaj.getArticle().getId() + "&displayEncherir=0");
				}

			} catch (NumberFormatException | BusinessException e) {
				e.printStackTrace();
			}
		}

	}

	private void majCredit(Enchere enchereOld, Utilisateur enchereurPrecedent) throws BusinessException {
		enchereurPrecedent.setCredit(enchereurPrecedent.getCredit() + enchereOld.getMontant());
		UtilisateurManager.getInstance().update(enchereurPrecedent);
	}

	private void placerEnchere(Utilisateur utilisateurConnecte, ArticleVendu article, int montantEnchere)
			throws BusinessException {
		Enchere newEnchere = new Enchere(montantEnchere, article, utilisateurConnecte);
		EnchereManager.getInstance().inserer(newEnchere);
		// On met à jour le crédit utilisateur connecté
		utilisateurConnecte.setCredit(utilisateurConnecte.getCredit() - montantEnchere);
		UtilisateurManager.getInstance().update(utilisateurConnecte);
	}

	private void erreurCreditInsufisant(List<Integer> listeCodesErreurs, List<String> stringListeCodesErreurs) {
		System.err.println("Crédit insuffisant");
		listeCodesErreurs.add(CodesResultatServlets.ERREUR_CREDIT_INSUFFISANT);
		for (int codeErreur : listeCodesErreurs) {
			stringListeCodesErreurs.add(LecteurMessage.getMessageErreur(codeErreur));
		}
	}

	private void getListeCodesErreurs(List<Integer> listeCodesErreurs, List<String> stringListeCodesErreurs) {
		listeCodesErreurs.add(CodesResultatServlets.ERREUR_CHOISIR_UN_MONTANT);
		for (int codeErreur : listeCodesErreurs) {
			stringListeCodesErreurs.add(LecteurMessage.getMessageErreur(codeErreur));
		}
	}

	private Enchere getEnchere(Enchere enchere, int idArticle) {
		try {
			enchere = EnchereManager.getInstance().selectByIdTop(idArticle);
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return enchere;
	}

	private ArticleVendu getArticle(ArticleVendu article, int idArticle) {
		try {
			article = ArticleVenduManager.getInstance().selectById(idArticle);
		} catch (BusinessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return article;
	}

	private int getIdArticle(HttpServletRequest request, int idArticle) {
		try {
			idArticle = Integer.valueOf(request.getParameter("idArticle"));
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		}
		return idArticle;
	}

	private Enchere creerEnchereVide(Enchere enchere, int idArticle) {
		ArticleVendu article;
		try {
			article = ArticleVenduManager.getInstance().selectById(idArticle);
			enchere = new Enchere(article);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		return enchere;
	}

	private String dipatchEnchere(HttpServletRequest request, Enchere enchere, Utilisateur utilisateurConnecte,
			String url, int idArticle) {
		try {
			Enchere meilleurEnchere = EnchereManager.getInstance().selectByIdTop(idArticle);

			// Si enchère en cours
			if (enchere.getArticle().getDateFinEncheres().isAfter(LocalDateTime.now())
					&& enchere.getArticle().getDateDebutEncheres().isBefore(LocalDateTime.now())) {
				// Si acheteur
				if (enchere.getArticle().getUtilisateur().getId() != utilisateurConnecte.getId()) {
					// Si déja meilleur enchéreur -> impossible d'enchérir à nouveau
					if (meilleurEnchere.getUtilisateur().getId() == utilisateurConnecte.getId()) {
						request.setAttribute("displayEncherir", 0);
					} else {
						request.setAttribute("displayEncherir", 1);
					}
					url = "/WEB-INF/encherir.jsp";
					// Si vendeur
				} else {
					request.setAttribute("article", enchere.getArticle());
					url = "/WEB-INF/vendreArticle.jsp";
				}
				// Si enchère non débutée
			} else if (LocalDateTime.now().isBefore(enchere.getArticle().getDateDebutEncheres())) {
				// Si acheteur
				if (enchere.getArticle().getUtilisateur().getId() != utilisateurConnecte.getId()) {
					request.setAttribute("displayAnulerVente", 1);
					url = "/WEB-INF/encherir.jsp";
					// Si vendeur
				} else {
					url = "/WEB-INF/vendreArticle.jsp";
				}
				// Si enchère terminée
			} else {
				// Si acheteur
				if (enchere.getArticle().getUtilisateur().getId() != utilisateurConnecte.getId()) {
					url = "/WEB-INF/acquisition.jsp";
					// Si vendeur
				} else {
					url = "/WEB-INF/detailMaVenteFinEnchere.jsp";
				}
			}
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		return url;
	}

}
