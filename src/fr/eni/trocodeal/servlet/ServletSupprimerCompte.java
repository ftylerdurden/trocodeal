package fr.eni.trocodeal.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.trocodeal.bll.UtilisateurManager;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Servlet implementation class ServletSupprimerCompte
 */
@WebServlet("/supprimerCompte")
public class ServletSupprimerCompte extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletSupprimerCompte() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Utilisateur utilisateur = null;

		int idUtilisateur = 0;
		if(session.getAttribute("utilisateur") != null) {
			utilisateur = (Utilisateur) session.getAttribute("utilisateur");
			idUtilisateur = utilisateur.getId();
		}
	    try {
	    	UtilisateurManager.getInstance().delete(idUtilisateur);
		} catch (BusinessException be) {
			be.printStackTrace();
		}

	    session.invalidate();
		this.getServletContext().getRequestDispatcher("/index").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
