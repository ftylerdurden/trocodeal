package fr.eni.trocodeal.servlet;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.trocodeal.bll.ArticleVenduManager;
import fr.eni.trocodeal.bo.ArticleVendu;
import fr.eni.trocodeal.bo.Categorie;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Servlet implementation class ServletListeEnchereMesVentes
 */
@WebServlet("/listeEncheresMesVentes")
public class ServletListeEncheresMesVentes extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Utilisateur utilisateurConnecte = (Utilisateur) request.getSession().getAttribute("utilisateur");
		LocalDateTime dateDuJour = LocalDateTime.now();
		List<Categorie> categories = new ArrayList<>();
		List<ArticleVendu> mesVentes = new ArrayList<>();
		int mesVentesFiltre = 1;
		try {
			mesVentesFiltre = Integer.parseInt(request.getParameter("mesVentesFiltre"));
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		}
		switch (mesVentesFiltre) {
		case 2:
			try {
				mesVentes = ArticleVenduManager.getInstance().listerMesVentesNonDebutees(utilisateurConnecte.getId());
			} catch (BusinessException e) {
				e.printStackTrace();
			}
			break;
		case 3:
			try {
				mesVentes = ArticleVenduManager.getInstance().listerMesVentesTerminees(utilisateurConnecte.getId());
			} catch (BusinessException e) {
				e.printStackTrace();
			}
			break;
		default:
			try {
				mesVentes = ArticleVenduManager.getInstance().listerMesVentesEnCours(utilisateurConnecte.getId());
			} catch (BusinessException e) {
				e.printStackTrace();
			}
		}
		request.setAttribute("ventesEncours", mesVentes);
		request.setAttribute("categories", categories);
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/listeEncheresMesVentes.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
