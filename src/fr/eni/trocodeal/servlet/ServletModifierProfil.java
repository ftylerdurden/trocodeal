package fr.eni.trocodeal.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.trocodeal.bll.UtilisateurManager;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.exceptions.BusinessException;
import fr.eni.trocodeal.exceptions.resultats.CodesResultatServlets;
import fr.eni.trocodeal.messages.LecteurMessage;

/**
 * Servlet implementation class ServletModifierProfil
 */
@WebServlet("/modifierProfil")
public class ServletModifierProfil extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public ServletModifierProfil() {
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/modifierProfil.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");

		request.setCharacterEncoding("UTF-8");

		List<Integer> listeCodesErreurs = new ArrayList<>();
		List<String> StringListeCodesErreurs = new ArrayList<>();

		String pseudo = request.getParameter("pseudo");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String email = request.getParameter("email");
		String telephone = request.getParameter("telephone");
		String rue = request.getParameter("rue");
		String ville = request.getParameter("ville");
		String codePostal = request.getParameter("codePostal");


		conditionForm(session, listeCodesErreurs, utilisateur, pseudo, nom, prenom, email, telephone, rue, ville, codePostal);

		for (Integer i : listeCodesErreurs) {
			StringListeCodesErreurs.add(LecteurMessage.getMessageErreur(i));
		}

		if (listeCodesErreurs.size() > 0) {
			utilisateur = new Utilisateur(pseudo, nom, prenom, email, telephone, rue, ville, codePostal);
			request.setAttribute("utilisateur", utilisateur);
			request.setAttribute("StringListeCodesErreurs", StringListeCodesErreurs);
			doGet(request, response);
		} else {
			try {
				int id = utilisateur.getId();
				utilisateur = new Utilisateur(id, pseudo, nom, prenom, email, telephone, rue, codePostal, ville,
						codePostal);
				UtilisateurManager.getInstance().update(utilisateur);
				session.setAttribute("utilisateur", utilisateur);
				response.sendRedirect(request.getContextPath() + "/profil?id=" + id);
			} catch (BusinessException e) {
				e.printStackTrace();
				utilisateur = new Utilisateur(pseudo, nom, prenom, email, telephone, rue, codePostal, ville,
						codePostal);
				request.setAttribute("utilisateur", utilisateur);
				request.setAttribute("StringListeCodesErreurs", StringListeCodesErreurs);
				doGet(request, response);
			}
		}
	}

	/**
	 * @param session
	 * @param utilisateur
	 * @param listeCodesErreurs
	 * @param pseudo
	 * @param nom
	 * @param prenom
	 * @param email
	 * @param telephone
	 * @param rue
	 * @param ville
	 * @param codePostal
	 */
	private void conditionForm(HttpSession session, List<Integer> listeCodesErreurs, Utilisateur utilisateur,
			String pseudo, String nom, String prenom, String email, String telephone, String rue, String ville,
			String codePostal) {


		if (!pseudo.equalsIgnoreCase(utilisateur.getPseudo())) {
			pseudoForm(listeCodesErreurs, pseudo);
		}

		nomForm(listeCodesErreurs, nom);
		prenomForm(listeCodesErreurs, prenom);

		if (!email.equalsIgnoreCase(utilisateur.getEmail())) {
			emailForm(listeCodesErreurs, email);
		}

		telephoneForm(listeCodesErreurs, telephone);
		rueForm(listeCodesErreurs, rue);
		villeForm(listeCodesErreurs, ville);
		codePostalForm(listeCodesErreurs, codePostal);

	}

	/**
	 * @param listeCodesErreurs
	 * @param codePostal
	 */
	private void codePostalForm(List<Integer> listeCodesErreurs, String codePostal) {
		if (codePostal.trim().length() == 0) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_CP);
		} else if (!codePostal.matches("^\\d{5}$")) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_CARACTERES_CODEPOSTAL);
		}
	}

	/**
	 * @param listeCodesErreurs
	 * @param ville
	 */
	private void villeForm(List<Integer> listeCodesErreurs, String ville) {
		if (ville.trim().length() == 0) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_VILLE);
		} else if (!ville.matches("^\\w{1,30}$")) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_CARACTERES_VILLE);
		}
	}

	/**
	 * @param listeCodesErreurs
	 * @param rue
	 */
	private void rueForm(List<Integer> listeCodesErreurs, String rue) {
		if (rue.trim().length() == 0) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_RUE);
		} else if (!rue.matches("^[a-zA-Z0-9 ]{1,30}")) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_CARACTERES_RUE);
		}
	}

	/**
	 * @param listeCodesErreurs
	 * @param telephone
	 */
	private void telephoneForm(List<Integer> listeCodesErreurs, String telephone) {
		if (telephone.trim().length() == 0) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_TEL);
		} else if (!telephone.matches("\\d{2}.\\d{2}.\\d{2}.\\d{2}.\\d{2}")) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_CARACTERES_TELEPHONE);
		}
	}

	/**
	 * @param listeCodesErreurs
	 * @param email
	 */
	private void emailForm(List<Integer> listeCodesErreurs, String email) {
		if (!verificationEmail(email)) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_MAIL_2);
		} else if (!email.matches("[a-zA-Z0-9._-][a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}")) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_CARACTERES_MAIL);
		} else if (email.trim().length() == 0) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_MAIL);

		}
	}

	/**
	 * @param listeCodesErreurs
	 * @param prenom
	 */
	private void prenomForm(List<Integer> listeCodesErreurs, String prenom) {
		if ((prenom.length() == 0) || (prenom.length() > 20)) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_PRENOM);
		} else if (!prenom.matches("^\\w{1,30}$")) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_CARACTERES_PRENOM);
		}
	}

	/**
	 * @param listeCodesErreurs
	 * @param nom
	 */
	private void nomForm(List<Integer> listeCodesErreurs, String nom) {
		if ((nom.trim().length() == 0) || (nom.trim().length() > 20)) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_NOM);
		} else if (!nom.matches("^\\w{1,30}$")) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_CARACTERES_NOM);
		}
	}

	/**
	 * @param listeCodesErreurs
	 * @param pseudo
	 */
	private void pseudoForm(List<Integer> listeCodesErreurs, String pseudo) {
		if (!verificationPseudo(pseudo)) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_PSEUDO_2);
		} else if (!pseudo.matches("^\\w{1,30}$")) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_CARACTERES_PSEUDO);
		} else if ((pseudo.trim().length() == 0) || (pseudo.trim().length() > 20)) {
			listeCodesErreurs.add(CodesResultatServlets.ERREUR_PSEUDO_1);
		}
	}

	/**
	 * Methode en charge de la vérification de l'existance de l'email.
	 *
	 * @param email
	 * @return
	 */
	private Boolean verificationEmail(String email) {
		boolean resultat = false;
		try {
			resultat = UtilisateurManager.getInstance().verifEmail(email);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		return resultat;

	}

	/**
	 * Methode en charge de la vérification de l'existance du pseudo.
	 *
	 * @param pseudo
	 * @return
	 */
	private Boolean verificationPseudo(String pseudo) {
		boolean resultat = false;
		try {
			resultat = UtilisateurManager.getInstance().verifPseudo(pseudo);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		return resultat;

	}

}
