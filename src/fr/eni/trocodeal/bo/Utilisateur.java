package fr.eni.trocodeal.bo;

import java.io.Serializable;

/**
 * Classe en charge de definir le javabean Utilisateur.
 *
 * @author Laura DUFAUT, Fabien BIGUET, Claude LUSSEAU, David HUARD.
 * @version trocodeal - v1.0
 * @date 11 mai 2021
 */
public class Utilisateur implements Serializable {

	private static final long serialVersionUID = 2320652005864311442L;

	private int id;
	private String pseudo;
	private String nom;
	private String prenom;
	private String email;
	private String telephone;
	private String rue;
	private String codePostal;
	private String ville;
	private String motDePasse;
	private int credit;
	private short administrateur;

	/**
	 * Constructeur sans parametre.
	 */
	public Utilisateur() {
	}

	/**
	 * Constructeur.
	 *
	 * @param noUtilisateur
	 * @param pseudo
	 * @param nom
	 * @param prenom
	 * @param email
	 * @param telephone
	 * @param rue
	 * @param codePostal
	 * @param ville
	 * @param motDePasse
	 * @param credit
	 * @param administrateur
	 */
	public Utilisateur(int id, String pseudo, String nom, String prenom, String email, String telephone, String rue,
			String ville, String codePostal, String motDePasse, int credit, short administrateur) {
		this(pseudo, nom, prenom, email, telephone, rue, ville, codePostal, motDePasse, credit, administrateur);
		this.setId(id);

	}

	/**
	 * Constructeur.
	 *
	 * @param pseudo
	 * @param nom
	 * @param prenom
	 * @param email
	 * @param telephone
	 * @param rue
	 * @param codePostal
	 * @param ville
	 * @param motDePasse
	 * @param credit
	 * @param administrateur
	 */
	public Utilisateur(String pseudo, String nom, String prenom, String email, String telephone, String rue,
			String ville, String codePostal, String motDePasse, int credit, short administrateur) {
		this(pseudo, nom, prenom, email, telephone, rue, ville, codePostal, motDePasse);
		this.setCredit(credit);
		this.setAdministrateur(administrateur);
	}

	/**
	 * Constructeur.
	 *
	 * @param id
	 * @param pseudo
	 * @param nom
	 * @param prenom
	 * @param email
	 * @param telephone
	 * @param rue
	 * @param codePostal
	 * @param ville
	 * @param motDePasse
	 */
	public Utilisateur(int id, String pseudo, String nom, String prenom, String email, String telephone,
			String rue, String codePostal, String ville, String motDePasse) {
        this(pseudo, nom, prenom, email, telephone, rue, ville, codePostal, motDePasse);
        this.setId(id);
	}

	/**
	 * Constructeur.
	 *
	 * @param pseudo
	 * @param nom
	 * @param prenom
	 * @param email
	 * @param telephone
	 * @param rue
	 * @param codePostal
	 * @param ville
	 * @param motDePasse
	 */
	public Utilisateur(String pseudo, String nom, String prenom, String email, String telephone, String rue,
			String codePostal, String ville, String motDePasse) {
		this(pseudo, nom, prenom, email, telephone, rue, ville, codePostal);
		this.setMotDePasse(motDePasse);
	}

	/**
	 * Constructeur.
	 *
	 * @param id
	 * @param pseudo
	 * @param nom
	 * @param prenom
	 * @param email
	 * @param telephone
	 * @param rue
	 * @param codePostal
	 * @param ville
	 */
	public Utilisateur(int id, String pseudo, String nom, String prenom, String email, String telephone, String rue,
			String codePostal, String ville) {
		this(pseudo, nom, prenom, email, telephone, rue, ville, codePostal);
		this.setId(id);
	}

	/**
	 * Constructeur.
	 *
	 * @param pseudo
	 * @param nom
	 * @param prenom
	 * @param email
	 * @param telephone
	 * @param rue
	 * @param codePostal
	 * @param ville
	 */
	public Utilisateur(String pseudo, String nom, String prenom, String email, String telephone, String rue,
			String codePostal, String ville) {
		this.setPseudo(pseudo);
		this.setNom(nom);
		this.setPrenom(prenom);
		this.setEmail(email);
		this.setTelephone(telephone);
		this.setRue(rue);
		this.setVille(ville);
		this.setCodePostal(codePostal);
	}


	/**
	 * Constructeur.
	 *
	 * @param id
	 * @param motDePasse
	 */
	public Utilisateur(int id, String motDePasse) {
		this(motDePasse);
		this.setId(id);
	}

	/**
	 * Constructeur.
	 *
	 * @param motDePasse
	 */
	public Utilisateur(String motDePasse) {
		this.setMotDePasse(motDePasse);
	}



	/**
	 * Méthode en charge de récupérer la valeur id.
	 *
	 * @return the id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Méthode en charge de définir la valeur id.
	 *
	 * @param id
	 *            the id to set.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Méthode en charge de récupérer la valeur pseudo.
	 *
	 * @return the pseudo.
	 */
	public String getPseudo() {
		return pseudo;
	}

	/**
	 * Méthode en charge de définir la valeur pseudo.
	 *
	 * @param pseudo
	 *            the pseudo to set.
	 */
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	/**
	 * Méthode en charge de récupérer la valeur nom.
	 *
	 * @return the nom.
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Méthode en charge de définir la valeur nom.
	 *
	 * @param nom
	 *            the nom to set.
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Méthode en charge de récupérer la valeur prenom.
	 *
	 * @return the prenom.
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Méthode en charge de définir la valeur prenom.
	 *
	 * @param prenom
	 *            the prenom to set.
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Méthode en charge de récupérer la valeur email.
	 *
	 * @return the email.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Méthode en charge de définir la valeur email.
	 *
	 * @param email
	 *            the email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Méthode en charge de récupérer la valeur telephone.
	 *
	 * @return the telephone.
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * Méthode en charge de définir la valeur telephone.
	 *
	 * @param telephone
	 *            the telephone to set.
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * Méthode en charge de récupérer la valeur rue.
	 *
	 * @return the rue.
	 */
	public String getRue() {
		return rue;
	}

	/**
	 * Méthode en charge de définir la valeur rue.
	 *
	 * @param rue
	 *            the rue to set.
	 */
	public void setRue(String rue) {
		this.rue = rue;
	}

	/**
	 * Méthode en charge de récupérer la valeur codePostal.
	 *
	 * @return the codePostal.
	 */
	public String getCodePostal() {
		return codePostal;
	}

	/**
	 * Méthode en charge de définir la valeur codePostal.
	 *
	 * @param codePostal
	 *            the codePostal to set.
	 */
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * Méthode en charge de récupérer la valeur ville.
	 *
	 * @return the ville.
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * Méthode en charge de définir la valeur ville.
	 *
	 * @param ville
	 *            the ville to set.
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}

	/**
	 * Méthode en charge de récupérer la valeur motDePasse.
	 *
	 * @return the motDePasse.
	 */
	public String getMotDePasse() {
		return motDePasse;
	}

	/**
	 * Méthode en charge de définir la valeur motDePasse.
	 *
	 * @param motDePasse
	 *            the motDePasse to set.
	 */
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	/**
	 * Méthode en charge de récupérer la valeur credit.
	 *
	 * @return the credit.
	 */
	public int getCredit() {
		return credit;
	}

	/**
	 * Méthode en charge de définir la valeur credit.
	 *
	 * @param credit
	 *            the credit to set.
	 */
	public void setCredit(int credit) {
		this.credit = credit;
	}

	/**
	 * Méthode en charge de récupérer la valeur administrateur.
	 *
	 * @return the administrateur.
	 */
	public short getAdministrateur() {
		return administrateur;
	}

	/**
	 * Méthode en charge de définir la valeur administrateur.
	 *
	 * @param administrateur
	 *            the administrateur to set.
	 */
	public void setAdministrateur(short administrateur) {
		this.administrateur = administrateur;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + administrateur;
		result = prime * result + ((codePostal == null) ? 0 : codePostal.hashCode());
		result = prime * result + credit;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + id;
		result = prime * result + ((motDePasse == null) ? 0 : motDePasse.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = prime * result + ((pseudo == null) ? 0 : pseudo.hashCode());
		result = prime * result + ((rue == null) ? 0 : rue.hashCode());
		result = prime * result + ((telephone == null) ? 0 : telephone.hashCode());
		result = prime * result + ((ville == null) ? 0 : ville.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Utilisateur other = (Utilisateur) obj;
		if (administrateur != other.administrateur)
			return false;
		if (codePostal == null) {
			if (other.codePostal != null)
				return false;
		} else if (!codePostal.equals(other.codePostal))
			return false;
		if (credit != other.credit)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id != other.id)
			return false;
		if (motDePasse == null) {
			if (other.motDePasse != null)
				return false;
		} else if (!motDePasse.equals(other.motDePasse))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (pseudo == null) {
			if (other.pseudo != null)
				return false;
		} else if (!pseudo.equals(other.pseudo))
			return false;
		if (rue == null) {
			if (other.rue != null)
				return false;
		} else if (!rue.equals(other.rue))
			return false;
		if (telephone == null) {
			if (other.telephone != null)
				return false;
		} else if (!telephone.equals(other.telephone))
			return false;
		if (ville == null) {
			if (other.ville != null)
				return false;
		} else if (!ville.equals(other.ville))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Utilisateur [id=");
		builder.append(id);
		builder.append(", pseudo=");
		builder.append(pseudo);
		builder.append(", nom=");
		builder.append(nom);
		builder.append(", prenom=");
		builder.append(prenom);
		builder.append(", email=");
		builder.append(email);
		builder.append(", telephone=");
		builder.append(telephone);
		builder.append(", rue=");
		builder.append(rue);
		builder.append(", codePostal=");
		builder.append(codePostal);
		builder.append(", ville=");
		builder.append(ville);
		builder.append(", motDePasse=");
		builder.append(motDePasse);
		builder.append(", credit=");
		builder.append(credit);
		builder.append(", administrateur=");
		builder.append(administrateur);
		builder.append("]");
		return builder.toString();
	}

}
