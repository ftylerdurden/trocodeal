/**
 *
 */
package fr.eni.trocodeal.bo;

import java.time.LocalDateTime;

/**
 * Classe en charge
 *
 * @author Laura
 * @version trocodeal - v1.0
 * @date 11 mai 2021 - 14:38:56
 */
public class ArticleVendu {

	private int id;
	private String nom;
	private String description;
	private LocalDateTime dateDebutEncheres;
	private LocalDateTime dateFinEncheres;
	private int miseAPrix;
	private int prixVente;
	private String etatVente;

	private Utilisateur utilisateur;
	private Categorie categorie;
	private Retrait lieuRetrait;

	/**
	 * Constructeur.
	 */
	public ArticleVendu() {
		super();
	}

	/**
	 * Constructeur.
	 *
	 * @param id
	 * @param nom
	 * @param description
	 * @param dateDebutEncheres
	 * @param dateFinEncheres
	 * @param miseAPrix
	 * @param prixVente
	 * @param utilisateur
	 * @param categorie
	 * @param lieuRetrait
	 */
	public ArticleVendu(int id, String nom, String description, LocalDateTime dateDebutEncheres, LocalDateTime dateFinEncheres,
			int miseAPrix, int prixVente, Utilisateur utilisateur, Categorie categorie, Retrait lieuRetrait) {
		this(nom, description, dateDebutEncheres, dateFinEncheres, miseAPrix, prixVente, utilisateur, categorie);
		this.setId(id);
		this.setLieuRetrait(lieuRetrait);
	}

	/**
	 * @param nom
	 * @param description
	 * @param dateDebutEncheres
	 * @param dateFinEncheres
	 * @param miseAPrix
	 * @param prixVente
	 * @param utilisateur
	 * @param categorie
	 */
	public ArticleVendu(String nom, String description, LocalDateTime dateDebutEncheres, LocalDateTime dateFinEncheres,
			int miseAPrix, int prixVente, Utilisateur utilisateur, Categorie categorie) {
		super();
		this.setNom(nom);
		this.setDescription(description);
		this.setDateDebutEncheres(dateDebutEncheres);
		this.setDateFinEncheres(dateFinEncheres);
		this.setMiseAPrix(miseAPrix);
		this.setPrixVente(prixVente);
		this.setUtilisateur(utilisateur);
		this.setCategorie(categorie);
	}

	/**
	 * @param idArticle
	 * @param nom2
	 * @param description2
	 * @param dateDebutEnchere
	 * @param dateFinEnchere
	 * @param miseAPrix2
	 * @param utilisateur2
	 * @param categorie2
	 */
	public ArticleVendu(int id, String nom, String description, LocalDateTime dateDebutEnchere,
			LocalDateTime dateFinEnchere, int miseAPrix, Utilisateur utilisateur, Categorie categorie) {
		super();
		this.setId(id);
		this.setNom(nom);
		this.setDescription(description);
		this.setDateDebutEncheres(dateDebutEncheres);
		this.setDateFinEncheres(dateFinEncheres);
		this.setMiseAPrix(miseAPrix);
		this.setPrixVente(prixVente);
		this.setUtilisateur(utilisateur);
		this.setCategorie(categorie);
	}

	/**
	 * Getter pour id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter pour id.
	 *
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter pour nom.
	 *
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Setter pour nom.
	 *
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Getter pour description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Setter pour description.
	 *
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Getter pour dateDebutEncheres.
	 *
	 * @return the dateDebutEncheres
	 */
	public LocalDateTime getDateDebutEncheres() {
		return dateDebutEncheres;
	}

	/**
	 * Setter pour dateDebutEncheres.
	 *
	 * @param dateDebutEncheres the dateDebutEncheres to set
	 */
	public void setDateDebutEncheres(LocalDateTime dateDebutEncheres) {
		this.dateDebutEncheres = dateDebutEncheres;
	}

	/**
	 * Getter pour dateFinEncheres.
	 *
	 * @return the dateFinEncheres
	 */
	public LocalDateTime getDateFinEncheres() {
		return dateFinEncheres;
	}

	/**
	 * Setter pour dateFinEncheres.
	 *
	 * @param dateFinEncheres the dateFinEncheres to set
	 */
	public void setDateFinEncheres(LocalDateTime dateFinEncheres) {
		this.dateFinEncheres = dateFinEncheres;
	}

	/**
	 * Getter pour miseAPrix.
	 *
	 * @return the miseAPrix
	 */
	public int getMiseAPrix() {
		return miseAPrix;
	}

	/**
	 * Setter pour miseAPrix.
	 *
	 * @param miseAPrix the miseAPrix to set
	 */
	public void setMiseAPrix(int miseAPrix) {
		this.miseAPrix = miseAPrix;
	}

	/**
	 * Getter pour prixVente.
	 *
	 * @return the prixVente
	 */
	public int getPrixVente() {
		return prixVente;
	}

	/**
	 * Setter pour prixVente.
	 *
	 * @param prixVente the prixVente to set
	 */
	public void setPrixVente(int prixVente) {
		this.prixVente = prixVente;
	}

	/**
	 * Getter pour etatVente.
	 *
	 * @return the etatVente
	 */
	public String getEtatVente() {
		return etatVente;
	}

	/**
	 * Setter pour etatVente.
	 *
	 * @param etatVente the etatVente to set
	 */
	public void setEtatVente(String etatVente) {
		this.etatVente = etatVente;
	}

	/**
	 * Getter pour utilisateur.
	 *
	 * @return the utilisateur
	 */
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	/**
	 * Setter pour utilisateur.
	 *
	 * @param utilisateur the utilisateur to set
	 */
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * Getter pour categorie.
	 *
	 * @return the categorie
	 */
	public Categorie getCategorie() {
		return categorie;
	}

	/**
	 * Setter pour categorie.
	 *
	 * @param categorie the categorie to set
	 */
	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	/**
	 * Getter pour lieuRetrait.
	 *
	 * @return the lieuRetrait
	 */
	public Retrait getLieuRetrait() {
		return lieuRetrait;
	}

	/**
	 * Setter pour lieuRetrait.
	 *
	 * @param lieuRetrait the lieuRetrait to set
	 */
	public void setLieuRetrait(Retrait lieuRetrait) {
		this.lieuRetrait = lieuRetrait;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateDebutEncheres == null) ? 0 : dateDebutEncheres.hashCode());
		result = prime * result + ((dateFinEncheres == null) ? 0 : dateFinEncheres.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((etatVente == null) ? 0 : etatVente.hashCode());
		result = prime * result + id;
		result = prime * result + miseAPrix;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + prixVente;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArticleVendu other = (ArticleVendu) obj;
		if (dateDebutEncheres == null) {
			if (other.dateDebutEncheres != null)
				return false;
		} else if (!dateDebutEncheres.equals(other.dateDebutEncheres))
			return false;
		if (dateFinEncheres == null) {
			if (other.dateFinEncheres != null)
				return false;
		} else if (!dateFinEncheres.equals(other.dateFinEncheres))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (etatVente == null) {
			if (other.etatVente != null)
				return false;
		} else if (!etatVente.equals(other.etatVente))
			return false;
		if (id != other.id)
			return false;
		if (miseAPrix != other.miseAPrix)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prixVente != other.prixVente)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ArticleVendu [id=" + id + ", nom=" + nom + ", description=" + description + ", dateDebutEncheres="
				+ dateDebutEncheres + ", dateFinEncheres=" + dateFinEncheres + ", miseAPrix=" + miseAPrix
				+ ", prixVente=" + prixVente + ", etatVente=" + etatVente + ", utilisateur=" + utilisateur.getId()
				+ ", categorie=" + categorie.getLibelle() + ", lieuRetrait=" + lieuRetrait + "]";
	}

}
