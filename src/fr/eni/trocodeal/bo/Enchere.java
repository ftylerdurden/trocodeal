package fr.eni.trocodeal.bo;

import java.time.LocalDateTime;

public class Enchere {

	private int id;
	private LocalDateTime date;
	private int montant;
	private ArticleVendu article;
	private Utilisateur utilisateur;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the date
	 */
	public LocalDateTime getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	/**
	 * @return the montant
	 */
	public int getMontant() {
		return montant;
	}

	/**
	 * @param montant
	 *            the montant to set
	 */
	public void setMontant(int montant) {
		this.montant = montant;
	}

	/**
	 * @return the article
	 */
	public ArticleVendu getArticle() {
		return article;
	}

	/**
	 * @param article
	 *            the article to set
	 */
	public void setArticle(ArticleVendu article) {
		this.article = article;
	}

	/**
	 * @return the utilisateur
	 */
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	/**
	 * @param utilisateur
	 *            the utilisateur to set
	 */
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * @param id
	 * @param date
	 * @param montant
	 * @param article
	 * @param utilisateur
	 */
	public Enchere(int id, LocalDateTime date, int montant, ArticleVendu article, Utilisateur utilisateur) {
		super();
		this.setId(id);
		this.setDate(date);
		this.setMontant(montant);
		this.setArticle(article);
		this.setUtilisateur(utilisateur);
	}

	public Enchere() {
		super();
	}

	public Enchere(ArticleVendu article) {
		this(0, null, article.getMiseAPrix() == 0 ? 1 : article.getMiseAPrix() - 1, article, null);
	}

	public Enchere(int montantEnchere, ArticleVendu article, Utilisateur utilisateur) {
		this(0, LocalDateTime.now(), montantEnchere, article, utilisateur);
	}

	@Override
	public String toString() {
		String utilisateur = "";
		if (this.getUtilisateur() == null) {
			utilisateur = "utilisateur null";
		} else {
			utilisateur = String.valueOf(this.utilisateur.getId());
		}
		return "Enchere [id=" + id + ", date=" + date + ", montant=" + montant + ", article=" + article.getId()
				+ ", utilisateur=" + utilisateur + "]";
	}

}