package fr.eni.trocodeal.bo;

public class Retrait {

	private ArticleVendu article;
	private String rue;
	private String codePostal;
	private String ville;

	public Retrait(ArticleVendu article, String rue, String codePostal, String ville) {
		this.setArticle(article);
		this.setRue(rue);
		this.setCodePostal(codePostal);
		this.setVille(ville);
	}

	public Retrait(String rue, String codePostal, String ville) {
		this(null, rue, codePostal, ville);
	}

	public Retrait() {
		super();
	}

	public ArticleVendu getArticle() {
		return article;
	}

	public void setArticle(ArticleVendu article) {
		this.article = article;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	@Override
	public String toString() {
		return rue + "\n " + codePostal + " " + ville;
	}

}
