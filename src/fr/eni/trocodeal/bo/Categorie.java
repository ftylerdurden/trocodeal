package fr.eni.trocodeal.bo;

public class Categorie {

	private int id;
	private String libelle;

	public Categorie(int id, String libelle) {
		this.setId(id);
		this.setLibelle(libelle);
	}

	public Categorie() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public String toString() {
		return "Categorie [no_categorie=" + id + ", libelle=" + libelle + "]";
	}

}
