package fr.eni.trocodeal.bll;

import java.util.ArrayList;
import java.util.List;

import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.dal.DaoFactory;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Classe en charge de de la gesion des utilisateurs.
 *
 * @author Laura DUFAUT, Fabien BIGUET, Claude LUSSEAU, David HUARD.
 * @version trocodeal - v1.0
 * @date 11 mai 2021
 */
public class UtilisateurManager {

	private static UtilisateurManager instance;

	public static UtilisateurManager getInstance() {
		if (instance == null) {
			instance = new UtilisateurManager();
		}
		return instance;
	}

	public UtilisateurManager() {
	}

	/**
	 * @param utilisateur
	 * @throws BusinessException
	 */
	public void insert(Utilisateur utilisateur) throws BusinessException {
		try {
			DaoFactory.getInstance().getUtilisateurDAO().insert(utilisateur);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
	}


	/**
	 * @return Liste d'utilisateurs.
	 * @throws BusinessException.
	 */
	public List<Utilisateur> selectAll() throws BusinessException {
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
		try {
			utilisateurs = DaoFactory.getInstance().getUtilisateurDAO().selectAll();
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		return utilisateurs;
	}

	/**
	 * @param utilisateur
	 * @throws BusinessException
	 */
	public void update(Utilisateur utilisateur) throws BusinessException {
		try {
			DaoFactory.getInstance().getUtilisateurDAO().update(utilisateur);
		} catch (BusinessException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @param utilisateur
	 * @throws BusinessException
	 */
	public void updateMdp(Utilisateur utilisateur) throws BusinessException {
		try {
			DaoFactory.getInstance().getUtilisateurDAO().updateMdp(utilisateur);
		} catch (BusinessException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @param id
	 * @throws BusinessException
	 */
	public void delete(int id) throws BusinessException {
		try {
			DaoFactory.getInstance().getUtilisateurDAO().delete(id);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
	}

	public Utilisateur connexion(String email, String mdp) throws BusinessException {
		Utilisateur resultat = null;
		resultat = DaoFactory.getInstance().getUtilisateurDAO().login(email, mdp);
		return resultat;
	}

	/**
	 * @param email
	 * @return boolean
	 * @throws BusinessException
	 */
	public Boolean verifEmail(String email) throws BusinessException {
		boolean resultat = false;
		try {

			resultat = DaoFactory.getInstance().getUtilisateurDAO().verifEmail(email);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		return resultat;
	}

	/**
	 * @param pseudo
	 * @return boolean
	 * @throws BusinessException
	 */
	public Boolean verifPseudo(String pseudo) throws BusinessException {
		boolean resultat = false;
		try {
			resultat = DaoFactory.getInstance().getUtilisateurDAO().verifPseudo(pseudo);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		return resultat;
	}

	/**
	 * @return
	 */
	public Utilisateur selectById(int id) throws BusinessException {
		Utilisateur utilisateur = null;
		try {
			utilisateur = DaoFactory.getInstance().getUtilisateurDAO().selectById(id);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		return utilisateur;
	}

	/**
	 * @param email
	 * @return
	 */
	public String selectByEmail(String email) throws BusinessException{
		String mdp = DaoFactory.getInstance().getUtilisateurDAO().selectByEmail(email);
		return mdp;
	}



}
