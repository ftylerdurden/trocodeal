package fr.eni.trocodeal.bll;

import java.util.List;

import fr.eni.trocodeal.bo.Categorie;
import fr.eni.trocodeal.dal.DaoFactory;
import fr.eni.trocodeal.exceptions.BusinessException;

public class CategorieManager {

	private static CategorieManager instance;

	public static CategorieManager getInstance() {
		if (instance == null) {
			instance = new CategorieManager();
		}
		return instance;
	}

	private CategorieManager() {
	}

	public List<Categorie> selectAll() throws BusinessException {

		return DaoFactory.getInstance().getCategorieDAO().selectAll();
	}

	/**
	 * @param parseInt
	 * @return
	 */
	public Categorie selectById(int id) throws BusinessException {
		return DaoFactory.getInstance().getCategorieDAO().selectById(id);
	}

}
