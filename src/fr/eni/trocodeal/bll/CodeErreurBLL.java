package fr.eni.trocodeal.bll;

/**
 * Classe en charge de lister les codes erreurs liés à la BLL
 * 
 * @author Laura DUFAUT, Fabien BIGUET, Claude LUSSEAU, David HUARD.
 * @version trocodeal - v1.0
 *
 */
public abstract class CodeErreurBLL {

	public static final int AU_MOINS_UN_ELEMENT = 20_000;
	public static final int HEURE_OBLIGATOIRE = 20_001;
	public static final int DATE_OBLIGATOIRE = 20_002;
	public static final int IDENTIFICATION_ECHOUEE = 20_003;

}
