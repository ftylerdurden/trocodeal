package fr.eni.trocodeal.bll.test;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.trocodeal.bll.UtilisateurManager;
import fr.eni.trocodeal.exceptions.BusinessException;

/**
 * Servlet implementation class ServletTestBll
 */
@WebServlet("/ServletTestBll")
public class ServletTestBll extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UtilisateurManager manager;
//	private HashingSha hash;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletTestBll() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		insertUtilisateur();


		manager = new UtilisateurManager();
		try {
			manager.delete(176);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

//	private void insertUtilisateur() {
//
//		hash = new HashingSha();
//		String motDePasse = "eni35clod!";
//		String passwordToHash = motDePasse;
//        byte[] salt = null;
//		try {
//			salt = hash.getSalt();
//		} catch (NoSuchAlgorithmException e) {
//			e.printStackTrace();
//		}
//        String securePassword = hash.getSecurePassword(passwordToHash, salt);
//
//		Utilisateur utilisateur = new Utilisateur();
//		utilisateur.setPseudo("Claudio");
//		utilisateur.setNom("Lusseau");
//		utilisateur.setPrenom("Claude");
//		utilisateur.setEmail("claude.lusseau@yahoo.fr");
//		utilisateur.setTelephone("06 69 33 69 33");
//		utilisateur.setRue("1, rue Jeanne Malivel");
//		utilisateur.setVille("Rennes");
//		utilisateur.setCodePostal( "35000");
//		utilisateur.setMotDePasse(securePassword);
//
//
//		manager = new UtilisateurManager();
//
//		try {
//			manager.insert(utilisateur);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
}
