package fr.eni.trocodeal.bll;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import fr.eni.trocodeal.bo.ArticleVendu;
import fr.eni.trocodeal.bo.Enchere;
import fr.eni.trocodeal.bo.Utilisateur;
import fr.eni.trocodeal.dal.DaoFactory;
import fr.eni.trocodeal.exceptions.BusinessException;

public class EnchereManager {

	private static EnchereManager instance;

	public static EnchereManager getInstance() {
		if (instance == null) {
			instance = new EnchereManager();
		}
		return instance;
	}

	private EnchereManager() {
	}

	public List<Enchere> selectAll() throws BusinessException {
		List<Enchere> encheres = new ArrayList<Enchere>();
		encheres = DaoFactory.getInstance().getEnchereDao().selectAll();
		return encheres;
	}

	public Boolean inserer(Enchere nouvelleEnchere) throws BusinessException {
		Boolean resultat = DaoFactory.getInstance().getEnchereDao().insert(nouvelleEnchere);
		return resultat;
	}

	public Enchere selectByIdTop(int idArticle) throws BusinessException {
		return DaoFactory.getInstance().getEnchereDao().selectByIdTop(idArticle);
	}

	public Boolean update(Utilisateur utilisateurConnecte, int montantEnchere, LocalDateTime date, int idEnchere) {
		return DaoFactory.getInstance().getEnchereDao().update(utilisateurConnecte, montantEnchere, date, idEnchere);
	}

	public List<ArticleVendu> selectMesEncheres(int idUtilisateur) throws BusinessException {
		List<Enchere> listeEncheres = DaoFactory.getInstance().getEnchereDao().selectMesEncheres(idUtilisateur);
		List<ArticleVendu> articlesListe = new ArrayList<>();
		for (Enchere enchere : listeEncheres) {
			articlesListe.add(enchere.getArticle());
		}
		return articlesListe;
	}

	public List<ArticleVendu> selectEncheresRemportees(int idUtilisateur) throws BusinessException {
		List<ArticleVendu> listeEncheresRemportees = new ArrayList<>();
		List<Enchere> listeEncheres = DaoFactory.getInstance().getEnchereDao()
				.selectEncheresEnCoursUtilisateur(idUtilisateur);
		for (Enchere enchere : listeEncheres) {
			Enchere meilleurEnchere = selectByIdTop(enchere.getArticle().getId());
			if (meilleurEnchere.getUtilisateur().getId() == idUtilisateur) {
				listeEncheresRemportees.add(enchere.getArticle());
			}
		}
		return listeEncheresRemportees;
	}

}
