package fr.eni.trocodeal.bll;

import java.util.ArrayList;
import java.util.List;

import fr.eni.trocodeal.bo.ArticleVendu;
import fr.eni.trocodeal.dal.DaoFactory;
import fr.eni.trocodeal.exceptions.BusinessException;

public class ArticleVenduManager {
	private static ArticleVenduManager instance;

	public static ArticleVenduManager getInstance() {
		if (instance == null) {
			instance = new ArticleVenduManager();
		}
		return instance;
	}

	private ArticleVenduManager() {
	}

	public List<ArticleVendu> selectAll() throws BusinessException {
		List<ArticleVendu> articleVendus = new ArrayList<ArticleVendu>();
		articleVendus = DaoFactory.getInstance().getArticleVenduDao().selectAll();
		return articleVendus;
	}

	public ArticleVendu insert(ArticleVendu articleVendu) throws BusinessException {
		try {
			DaoFactory.getInstance().getArticleVenduDao().insert(articleVendu);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		return articleVendu;
	}

	public void delete(int id) throws BusinessException {
		DaoFactory.getInstance().getArticleVenduDao().delete(id);
	}

	public List<ArticleVendu> rechercherArticle(String recherche, String categorie) throws BusinessException {
		return DaoFactory.getInstance().getArticleVenduDao().searchArticle(recherche, categorie);
	}

	public ArticleVendu selectById(int idArticle) throws BusinessException {
		return DaoFactory.getInstance().getArticleVenduDao().selectById(idArticle);
	}

	public List<ArticleVendu> selectAllOpen() throws BusinessException {
		List<ArticleVendu> articleVendus = new ArrayList<ArticleVendu>();
		articleVendus = DaoFactory.getInstance().getArticleVenduDao().selectAllOpen();
		return articleVendus;
	}

	public List<ArticleVendu> listerMesVentesEnCours(int id) throws BusinessException {
		List<ArticleVendu> articleVendus = new ArrayList<ArticleVendu>();
		articleVendus = DaoFactory.getInstance().getArticleVenduDao().listerMesVentesEnCours(id);
		return articleVendus;
	}

	public List<ArticleVendu> listerMesVentesNonDebutees(int id) throws BusinessException {
		List<ArticleVendu> articleVendus = new ArrayList<ArticleVendu>();
		articleVendus = DaoFactory.getInstance().getArticleVenduDao().listerMesVentesNonDebutess(id);
		return articleVendus;
	}

	public List<ArticleVendu> listerMesVentesTerminees(int id) throws BusinessException {
		List<ArticleVendu> articleVendus = new ArrayList<ArticleVendu>();
		articleVendus = DaoFactory.getInstance().getArticleVenduDao().listerMesVentesTerminees(id);
		return articleVendus;
	}

	/**
	 * @param articleVendu
	 */
	public ArticleVendu updade(ArticleVendu articleVendu) throws BusinessException {
		try {
			articleVendu = DaoFactory.getInstance().getArticleVenduDao().update(articleVendu);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		return articleVendu;

	}
}